package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;
import org.junit.*;

import static org.junit.Assert.*;

public class TestMove {

    @Test
    public void testRegularMoveToString(){
        Move move = Move.regular(FigureType.ROOK, Pos.parse("e2"), Pos.parse("e4"));
        String str = move.toString(), exp = "Re2e4";
        assertEquals(exp, str);
    }

    @Test
    public void testPoneMoveToString(){
        Move move = Move.regular(FigureType.PONE, Pos.parse("e2"), Pos.parse("e4"));
        String str = move.toString(), exp = "e2e4";
        assertEquals(exp, str);
    }

    @Test
    public void testPoneTransformMoveToString(){
        Move move = Move.poneTransformation(Pos.parse("e2"), Pos.parse("e4"), false);
        String defaultStr = move.toString(), defaultExp = "e2e4Q";

        move.setPoneTransformationFigure(FigureType.ROOK);
        String customStr = move.toString(), customExp = "e2e4R";

        assertEquals(defaultExp, defaultStr);
        assertEquals(customExp, customStr);
    }

    @Test
    public void testCastleToString(){
        Move longCastle = Move.castle(Pos.parse("e1"), Pos.parse("c1"), Pos.parse("a1"), Pos.parse("d1"));
        Move shortCastle = Move.castle(Pos.parse("e1"), Pos.parse("g1"), Pos.parse("h1"), Pos.parse("f1"));
        String longCastleStr = longCastle.toString();
        String shortCastleStr = shortCastle.toString();
        assertEquals("Assert short castle", "0-0", shortCastleStr);
        assertEquals("Assert long castle", "0-0-0", longCastleStr);
    }

    @Test
    public void testRegularMoveParse(){
        testParse(Move.regular(FigureType.ROOK, Pos.parse("e2"), Pos.parse("e4")));
    }

    @Test
    public void testPoneMoveParse(){
        testParse(Move.regular(FigureType.PONE, Pos.parse("e2"), Pos.parse("e4")));
    }

    @Test
    public void testEnPassantParse(){
        Move move = Move.enPassant(Pos.parse("e5"), Pos.parse("d6"), Pos.parse("d5")); //white take
        Move parsed = testParse(move);
        assertEquals(move.getEnPassantTarget(), parsed.getEnPassantTarget());
    }

    @Test
    public void testPoneTransformParse(){
        testParse(Move.poneTransformation(Pos.parse("e7"), Pos.parse("e8"), false));
    }

    private Move testParse(Move move){
        Move parsed = Move.parseMove(move.toString(), true);
        assertNotNull(parsed);
        assertEquals(parsed.toString(), move.toString());
        return parsed;
    }
}
