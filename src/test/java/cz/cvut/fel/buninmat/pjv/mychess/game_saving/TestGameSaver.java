package cz.cvut.fel.buninmat.pjv.mychess.game_saving;

import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import cz.cvut.fel.buninmat.pjv.mychess.model.ClocksModel;
import cz.cvut.fel.buninmat.pjv.mychess.model.WriterModel;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class TestGameSaver {

    @Test
    public void testBoardSaveLoad(){
        BoardModel bm = new BoardModel();
        BoardModel res = saveLoad(bm);
        assertEquals(bm.board, res.board);
    }

    @Test
    public void testClocksSaveLoad(){
        BoardModel bm = new BoardModel();
        bm.addClocks(ClocksModel.standard(13, 3), ClocksModel.standard(13, 3));
        BoardModel res = saveLoad(bm);
        if(bm.hasClocks()){
            assertEquals(bm.getWhiteClocks(), res.getWhiteClocks());
            assertEquals(bm.getBlackClocks(), res.getBlackClocks());
        }else{
            assertFalse(res.hasClocks());
        }
    }

    @Test
    public void testMovesSaveLoad(){
        BoardModel bm = new BoardModel();
        WriterModel wm = new WriterModel();
        bm.addWriter(wm);
        wm.postMessage("hello");
        wm.postMessage("e2e4", true);
        wm.postMessage("e7e5", false);
        BoardModel res = saveLoad(bm);
        assertTrue("Move messages are not equal", moveMessagesEqual(res.getWriterModel().getMoveMessages(), wm.getMoveMessages()));
    }

    private BoardModel saveLoad(BoardModel bm){

        StringWriter sw = new StringWriter();
        BufferedWriter bw = new BufferedWriter(sw);
        //write board to a buffer
        GameSaver.writeToBuffer(bw, bm);
        //flush to a string
        try {
            bw.flush();
        }catch (IOException e){
            Assert.fail(String.format("Exception while flushing buffer %s", e.getMessage()));
        }
        String saved = sw.toString();
        //string to stream
        InputStream stream = new ByteArrayInputStream(saved.getBytes(StandardCharsets.UTF_8));
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        //read model from string
        return GameSaver.readFromBuffer(br);

    }

    private boolean moveMessagesEqual(List<WriterModel.MoveMessage> first, List<WriterModel.MoveMessage> second){
        ArrayList<WriterModel.MoveMessage> arr1 = getMoveMessages(first);
        ArrayList<WriterModel.MoveMessage> arr2 = getMoveMessages(second);
        if(arr1.size() != arr2.size()){
            return false;
        }
        for(int i = 0; i < arr1.size(); i++){
            WriterModel.MoveMessage m1 = arr1.get(i);
            WriterModel.MoveMessage m2 = arr2.get(i);
            if(m1.type != m2.type || !m1.content.equals(m2.content)){
                return false;
            }
        }
        return true;
    }

    private ArrayList<WriterModel.MoveMessage> getMoveMessages(List<WriterModel.MoveMessage> moveMessages){
        ArrayList<WriterModel.MoveMessage> res = new ArrayList<>();
        for(WriterModel.MoveMessage mm : moveMessages){
            if(mm.type != WriterModel.MoveMessageType.NEUTRAL){
                res.add(mm);
            }
        }
        return res;
    }
}
