package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

import java.util.LinkedList;

public class King extends Figure {
    @Override
    public FigureType getType() {
        return FigureType.KING;
    }

    static Pos[] moveDirs = new Pos[] {
            new Pos(-1, 1), new Pos(0, 1), new Pos(1, 1),
            new Pos(1, 0), new Pos(1, -1), new Pos(0, -1),
            new Pos(-1, -1), new Pos(-1, 0)
    };

    King(Boolean white) {
        super(white);
    }

    @Override
    public LinkedList<Move> getMoves(Board board, Pos from) {
        Figure figure = board.getFigure(from);
        LinkedList<Pos> attacks = getAttacks(from, board);
        LinkedList<Move> moves = new LinkedList<>();
        for(Pos to : attacks){
            if(board.hasAttack(to)){
                continue;
            }
            Figure target = board.getFigure(to);
            if(target == null || target.isWhite != figure.isWhite){
                moves.add(Move.regular(getType(), from, to));
            }
        }
        if(board.isCheck()){
            //no castle from under a check
            return moves;
        }
        boolean shortCastleOpen = free(board, from.right()) && free(board, from.right(2)) &&
                ! board.hasAttack(from.right()) && ! board.hasAttack(from.right(2));

        boolean longCastleOpen = free(board, from.left()) && free(board, from.left(2)) && free(board, from.left(3)) &&
                ! board.hasAttack(from.left()) && ! board.hasAttack(from.left(2));

        Move shortCastle = Move.castle(from, from.right(2), from.right(3), from.right());
        Move longCastle = Move.castle(from, from.left(2), from.left(4), from.left());
        if(figure.isWhite){
            if(board.shortCastleWhite && shortCastleOpen){
                moves.add(shortCastle);
            }
            if(board.longCastleWhite && longCastleOpen){
                moves.add(longCastle);
            }
        }else{
            if(board.shortCastleBlack && shortCastleOpen){
                moves.add(shortCastle);
            }
            if(board.longCastleBlack && longCastleOpen){
                moves.add(longCastle);
            }
        }
        return moves;
    }

    private boolean free(Board board, Pos pos){
        return !pos.nonBoard() && board.getFigure(pos) == null;
    }

    @Override
    public LinkedList<Pos> getAttacks(Pos from, Board board) {
        return attacksFromDirs(from, moveDirs);
    }
}
