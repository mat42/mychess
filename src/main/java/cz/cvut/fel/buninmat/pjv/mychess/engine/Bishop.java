package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

import java.util.LinkedList;

public class Bishop extends Figure {
    @Override
    public FigureType getType() {
        return FigureType.BISHOP;
    }

    Bishop(Boolean white) {
        super(white);
    }

    @Override
    public LinkedList<Move> getMoves(Board board, Pos from) {
        return movesFromAttacksLine(from, board);
    }

    @Override
    public LinkedList<Pos> getAttacks(Pos from, Board board) {
        return attacksLineFromDirs(from,
                new Pos[]{new Pos(-1, -1), new Pos(1, 1), new Pos(-1, 1), new Pos(1, -1)},
                board);
    }
}
