package cz.cvut.fel.buninmat.pjv.mychess.opponent;

import cz.cvut.fel.buninmat.pjv.mychess.engine.Move;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;

/**
 * Abstract opponent player
 */
public interface Opponent {
    /**
     * Get a move to make in the boardModel
     */
    Move getMove(BoardModel board);

    interface OnMoveAction{
        void run(Move move);
    }

    /**
     * Get the move asynchronously
     */
    void getMoveAsync(BoardModel board, OnMoveAction onMove);
}
