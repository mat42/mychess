package cz.cvut.fel.buninmat.pjv.mychess.view;

import cz.cvut.fel.buninmat.pjv.mychess.engine.FigureType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * User control for displaying taken figures
 */
public class TakenFiguresControl extends HBox {

    private boolean isWhite;

    private ArrayList<FigureType> figures;

    private int figureSize;

    public TakenFiguresControl(boolean isWhite){
        this.isWhite = isWhite;
        figureSize = 30;
        figures = new ArrayList<>();
        setPrefHeight(30);
    }

    public void setFigures(LinkedList<FigureType> newFigures){
        figures.clear();
        figures.addAll(newFigures);
        figures.sort((f1, f2) -> {
            if(f1.ordinal() == f2.ordinal()){
                return 0;
            }
            return f1.ordinal() > f2.ordinal()? 1 : -1;
        });
        getChildren().clear();
        for(FigureType fig : figures){
            ImageView imageView = new ImageView(isWhite? fig.whiteImageUrl : fig.blackImageUrl);
            imageView.setFitWidth(figureSize);
            imageView.setFitHeight(figureSize);
            getChildren().add(imageView);
        }
    }
}
