package cz.cvut.fel.buninmat.pjv.mychess.code;

/**
 * Represents position on chess board
 */
public class Pos {
    public final int x, y;
    public Pos(int x, int y){
        this.x = x;
        this.y = y;
    }

    public boolean is(Pos other){
        return x == other.x && y == other.y;
    }

    public boolean is(String posRepr){
        Pos other = parse(posRepr);
        if(other == null){
            return false;
        }
        return other.x == x && other.y == y;
    }

    /**
     * Parses position from standard chess notation
     * @param posRepr string representing position in standard chess notation (e.g. "a1")
     * @return Pos
     */
    public static Pos parse(String posRepr){
        posRepr = posRepr.toLowerCase();
        int x = posRepr.charAt(0) - 'a';
        int y = posRepr.charAt(1) - '1';
        if(x < 0 || x > 7 || y < 0 || y > 7) {
            return null;
        }
        return new Pos(x, y);
    }

    @Override
    public String toString(){
        return String.format("%c%d", x + 'a', y + 1);
    }

    @Override
    public boolean equals(Object object){
        if(object.getClass() != getClass()){
            return false;
        }
        Pos other = (Pos)object;
        return x == other.x && y == other.y;
    }

    public Pos plus(Pos other){
        return new Pos(x + other.x, y + other.y);
    }

    public boolean nonBoard(){
        return x < 0 || x > 7 || y < 0 || y > 7;
    }

    public Pos left(){
        return left(1);
    }

    public Pos right(){
        return right(1);
    }

    public Pos left(int scal){
        return new Pos(x - scal, y);
    }

    public Pos right(int scal){
        return new Pos(x + scal, y);
    }

    public Pos flipX(){return new Pos(-x, y);}

    public Pos flipY(){return new Pos(x, -y);}

    public Pos flipXY(){return new Pos(-x, -y);}
}
