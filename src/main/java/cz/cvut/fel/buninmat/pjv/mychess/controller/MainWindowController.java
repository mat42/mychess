package cz.cvut.fel.buninmat.pjv.mychess.controller;

import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;
import cz.cvut.fel.buninmat.pjv.mychess.Main;
import cz.cvut.fel.buninmat.pjv.mychess.network.Communicator;
import cz.cvut.fel.buninmat.pjv.mychess.opponent.SimpleOpponent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.logging.Logger;

/**
 * Controller for MainWindow.fxml. Is always active and launches other application windows.
 */
public class MainWindowController {

    @FXML
    private GridPane mainGrid;

    @FXML
    private Button localGameButton;

    @FXML
    private Button AIGameButton;

    @FXML
    private Button networkGameButton;

    @FXML
    private Button loginButton;

    private void fitImage(ImageView imageView, int size){
        imageView.setFitHeight(size);
        imageView.setFitWidth(size);
    }

    private static final Logger LOG = Logger.getLogger(Communicator.class.getName());


    @FXML
    private void initialize(){
        mainGrid.getStylesheets().add("styles/common.css");
        mainGrid.getStylesheets().add("styles/main-window.css");
        localGameButton.setOnMouseClicked(event-> Main.getInstance().showCreateGameWindow());
        AIGameButton.setOnMouseClicked(event -> createAIGame());
        networkGameButton.setOnMouseClicked(event -> createNetworkGame());
        loginButton.setOnMouseClicked(event -> {
            Main.getInstance().showLoginDialog(getStage(), this::onLogin);
        });

        ImageView poneImage = new ImageView(new Image("icons/pone_icon.png"));
        ImageView computerImage = new ImageView(new Image("icons/computer_icon.png"));
        ImageView networkImage = new ImageView(new Image("icons/network_icon.png"));
        fitImage(poneImage, 80);
        fitImage(computerImage, 80);
        fitImage(networkImage, 80);
        localGameButton.setGraphic(poneImage);
        AIGameButton.setGraphic(computerImage);
        networkGameButton.setGraphic(networkImage);

        Communicator.getInstance().waitForOpponent(opponent -> {
            LOG.info("Game request");
            //TODO: ask for user permission to accept
            opponent.getGameAsync(boardModel -> {
                Main.getInstance().showGameWindow(boardModel);
            });
        });
    }

    private Stage getStage(){
        return (Stage) mainGrid.getScene().getWindow();
    }

    private void createAIGame(){
        Main.getInstance().showCreateGameWindow(new SimpleOpponent());
    }

    private void createNetworkGame(){
        Communicator.getInstance().connectToOpponent(opponent -> {
            LOG.info("Opponent reached");
            Main.getInstance().showCreateGameWindow(opponent);
        });
    }

    private void onLogin(Player player){
        Main.getInstance().setUser(player);
        loginButton.setText(player.getNick());
        loginButton.setOnMouseClicked(event -> {
            Main.getInstance().showProfileWindow(player);
        });
    }
}
