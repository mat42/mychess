package cz.cvut.fel.buninmat.pjv.mychess.engine;

/**
 * Types of chess pieces and parameters of each type including constructor references and paths to pictures of pieces.
 */
public enum FigureType {
    KING("K", King::new, 1, "icons/king_w.png", "icons/king_b.png"),
    QUEEN("Q", Queen::new, 1, "icons/queen_w.png", "icons/queen_b.png"),
    ROOK("R", Rook::new, 2, "icons/rook_w.png", "icons/rook_b.png"),
    BISHOP("B", Bishop::new, 2, "icons/bishop_w.png", "icons/bishop_b.png"),
    KNIGHT("N", Knight::new, 2, "icons/knight_w.png", "icons/knight_b.png"),
    PONE("", Pone::new, 8, "icons/pone_w.png", "icons/pone_b.png");

    private FigureFactory figureFactory;
    public final String whiteImageUrl;
    public final String blackImageUrl;
    public final int maxCount;
    public final String name;

    private interface FigureFactory {
        Figure make(Boolean white);
    }

    FigureType(String name, FigureFactory figureFactory, int maxCount, String whiteImageUrl, String blackImageUrl) {
        this.name = name;
        this.maxCount = maxCount;
        this.figureFactory = figureFactory;
        this.whiteImageUrl = whiteImageUrl;
        this.blackImageUrl = blackImageUrl;
    }

    public static FigureType parse(String name){
        if(name.equals("")){
            return FigureType.PONE;
        }
        for(FigureType figureType:values()){
            if(figureType.name.equals(name))
                return figureType;
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Creates a figure instance of corresponding type and specified color
     * @param white color of the desired figure
     */
    public Figure makeInstance(boolean white) {
        return figureFactory.make(white);
    }

    public String getImageUrl(boolean isWhite) {
        return isWhite ? whiteImageUrl : blackImageUrl;
    }
}