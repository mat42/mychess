package cz.cvut.fel.buninmat.pjv.mychess.view.board;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Figure;
import cz.cvut.fel.buninmat.pjv.mychess.engine.FigureType;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Move;
import cz.cvut.fel.buninmat.pjv.mychess.opponent.Opponent;
import cz.cvut.fel.buninmat.pjv.mychess.view.TakenFiguresControl;

import java.util.LinkedList;

/**
 * Board control for the game itself
 */
public class GameBoardUI extends BoardUI {

    private TakenFiguresControl whiteTakenFigures, blackTakenFigures;
    private HBox whiteFigureSelector, blackFigureSelector;

    private HBox whiteFigureSelectBox, blackFigureSelectBox;
    private Button whiteSelectBoxSpacer, blackSelectBoxSpacer;

    private boolean outOfTime;

    public GameBoardUI(int width, int height, BoardModel boardModel){
        super(width, height, boardModel);
        boardModel.setOutOfTimeCallback(()->{
            outOfTime = true;
            hideMoves();
        });
        initTakenFiguresPanels();
        initFigureSelectPane();

        TakenFiguresControl topTakenFigures, bottomTakenFigures;
        HBox topSelectBox, bottomSelectBox;
        if(boardModel.opponentWhite()){
            topSelectBox = blackFigureSelectBox;
            topTakenFigures = blackTakenFigures;
            bottomSelectBox = whiteFigureSelectBox;
            bottomTakenFigures = whiteTakenFigures;
        }else{
            topSelectBox = whiteFigureSelectBox;
            topTakenFigures = whiteTakenFigures;
            bottomSelectBox = blackFigureSelectBox;
            bottomTakenFigures = blackTakenFigures;
        }
        getChildren().add(topTakenFigures);
        getChildren().add(topSelectBox);
        getChildren().add(mainGrid);
        getChildren().add(bottomSelectBox);
        getChildren().add(bottomTakenFigures);

        initOpponent();
    }

    private void initTakenFiguresPanels(){
        whiteTakenFigures = new TakenFiguresControl(true);
        whiteTakenFigures.setAlignment(javafx.geometry.Pos.TOP_LEFT);
        blackTakenFigures = new TakenFiguresControl(false);
        blackTakenFigures.setAlignment(javafx.geometry.Pos.BOTTOM_LEFT);
    }

    private void initFigureSelectPane(){

        whiteFigureSelectBox = new HBox();
        blackFigureSelectBox = new HBox();

        whiteSelectBoxSpacer = new Button();
        whiteSelectBoxSpacer.setVisible(false);

        blackSelectBoxSpacer = new Button();
        blackSelectBoxSpacer.setVisible(false);

        whiteFigureSelector = getFigureSelector(true);
        blackFigureSelector = getFigureSelector(false);
        whiteFigureSelector.setVisible(false);
        blackFigureSelector.setVisible(false);

        whiteFigureSelectBox.getChildren().add(whiteSelectBoxSpacer);
        whiteFigureSelectBox.getChildren().add(whiteFigureSelector);
        blackFigureSelectBox.getChildren().add(blackSelectBoxSpacer);
        blackFigureSelectBox.getChildren().add(blackFigureSelector);
    }

    @Override
    void addFieldHandlers(Node field, int i, int j){
        field.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> onFieldPress(i, j));
    }

    private LinkedList<Move> moves;
    private Button[] moveFields;
    private String[] defaultStyles;
    private Button activeFigure;
    private String figureDefaultStyle;

    private boolean choosingFigure;

    private void onFieldPress(int i, int j){

        if(choosingFigure || boardModel.board.isStall() || outOfTime){
            return;
        }

        hideMoves();

        if(moves != null) {
            for (Move move : moves) {
                if (move.getTo().is(toBoardPos(i, j))) {
                    makeMove(move);
                    activeFigure = null;
                    moves = null;
                    return;
                }
            }
            moves = null;
        }

        Button newActive = getField(i, j);
        if(newActive == activeFigure){
            activeFigure = null;
            return;
        }

        Figure fig = boardModel.board.getFigure(toBoardPos(i, j));
        if(boardModel.getOpponent() != null &&
                fig != null &&
                fig.isWhite == boardModel.opponentWhite()){
            //opponent`s figure
            return;
        }

        moves = boardModel.board.getMoves(toBoardPos(i, j));
        if(moves == null){
            activeFigure = null;
            return;
        }
        activeFigure = newActive;
        ObservableList<String> styleClass = activeFigure.getStyleClass();
        figureDefaultStyle = styleClass.remove(1);
        styleClass.add(getActiveStyle(i, j));

        moveFields = new Button[moves.size()];
        defaultStyles = new String[moves.size()];
        for(int c = 0; c < moves.size(); c++){
            Pos fieldPos = fromBoardPos(moves.get(c).getTo());
            Button moveField = getField(fieldPos);
            moveFields[c] = moveField;
            styleClass = moveField.getStyleClass();
            defaultStyles[c] = styleClass.remove(1);
            styleClass.add(getActiveStyle(fieldPos.x, fieldPos.y));
        }
    }

    private void hideMoves(){
        if(activeFigure != null){
            activeFigure.getStyleClass().remove(1);
            activeFigure.getStyleClass().add(figureDefaultStyle);
            for(int c = 0; c < moveFields.length; c++){
                moveFields[c].getStyleClass().remove(1);
                moveFields[c].getStyleClass().add(defaultStyles[c]);
            }
        }
    }

    private void initOpponent(){
        if(boardModel.getOpponent() != null && boardModel.board.isWhiteTurn() == boardModel.opponentWhite()){
            makeOpponentMove(boardModel.getOpponent());
        }
    }

    private void makeMove(Move move) {
        makePlayerMove(move);
        if(choosingFigure){
            return;
        }
        Opponent opponent = boardModel.getOpponent();
        if(opponent != null){
            makeOpponentMove(opponent);
        }
    }

    private void makeOpponentMove(Opponent opponent){
        opponent.getMoveAsync(boardModel, opponentMove -> {
            if(opponentMove != null) {
                makePlayerMove(opponentMove);
            }
        });
    }

    private FigureType transformationFigure;
    private Move transformationMove;

    private void makePlayerMove(Move move){
        boolean whiteMove = boardModel.board.getFigure(move.getFrom()).isWhite;
        if(move.isPoneTransformation() && move.chooseTransformation()){
            if(transformationFigure == null){
                transformationMove = move;
                showFigureSelector(whiteMove, move.getTo().x);
                choosingFigure = true;
                return;
            }
            choosingFigure = false;
            move.setPoneTransformationFigure(transformationFigure);
            transformationFigure = null;
            transformationMove = null;
        }

        if(boardModel.board.isCheck()){
            setCheck(false);
        }

        boardModel.makeMove(move);

        displayMove(move.getFrom(), move.getTo());
        if(move.isCastle()){
            displayMove(move.getRookFrom(), move.getRookTo());
        }
        if(move.isEnPassant()){
            getField(fromBoardPos(move.getEnPassantTarget())).setGraphic(null);
        }
        blackTakenFigures.setFigures(boardModel.board.getWhiteCounterweight());
        whiteTakenFigures.setFigures(boardModel.board.getBlackCounterweight());

        if(boardModel.board.isCheck()){
            setCheck(true);
        }
    }

    private HBox getFigureSelector(boolean isWhite){
        HBox selector = new HBox();
        for(FigureType figure : new FigureType[]
                {FigureType.QUEEN, FigureType.ROOK, FigureType.BISHOP, FigureType.KNIGHT}){
            ImageView img = new ImageView(isWhite? figure.whiteImageUrl: figure.blackImageUrl);
            img.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                transformationFigure = figure;
                selector.setVisible(false);
                makeMove(transformationMove);
            });
            img.setFitHeight(getFigureSize()); img.setFitWidth(getFigureSize());
            selector.getChildren().add(img);
        }
        return selector;
    }

    private void showFigureSelector(boolean isWhite, int fieldX){
        if(boardModel.opponentWhite()){
            fieldX = 7 - fieldX;
        }
        HBox selector = isWhite? whiteFigureSelector : blackFigureSelector;
        selector.setVisible(true);
        Button spacer = isWhite? whiteSelectBoxSpacer : blackSelectBoxSpacer;
        spacer.setPrefWidth(getWidth() / 2 - (4 - fieldX) * getFieldSize() - getFieldSize() * 1.5);
    }

    private void setCheck(boolean active){
        Pos checkPos = fromBoardPos(boardModel.board.getCheckPos());
        ObservableList<String> styleClass = getField(checkPos).getStyleClass();
        styleClass.remove(1);
        String style = active? getCheckStyle(checkPos.x, checkPos.y) : getDefaultStyle(checkPos.x, checkPos.y);
        styleClass.add(style);
    }

    private void displayMove(Pos from, Pos to){
        Button fromField = getField(fromBoardPos(from));
        Pos gridTo = fromBoardPos(to);
        addFigure(gridTo.x, gridTo.y);
        fromField.setGraphic(null);
    }

}
