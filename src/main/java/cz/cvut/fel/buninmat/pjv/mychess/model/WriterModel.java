package cz.cvut.fel.buninmat.pjv.mychess.model;

import java.util.LinkedList;

/**
 * Manages writing and saving of game move strings and game messages
 */
public class WriterModel {
    public enum MoveMessageType{
        WHITE,
        BLACK,
        NEUTRAL
    }

    public static class MoveMessage{
        public String content;
        public MoveMessageType type;

        public MoveMessage(String content, MoveMessageType type) {
            this.content = content;
            this.type = type;
        }
    }

    private LinkedList<MoveMessage> moveMessages = new LinkedList<>();

    public interface DisplayMessageCallback{
        void run(String message, MoveMessageType type);
    }

    private DisplayMessageCallback messageCallback;

    /**
     * Called when new message is posted
     * @param callback receives the message string and type of message (black move, white move or just a message)
     */
    public void setMessageCallback(DisplayMessageCallback callback){
        messageCallback = callback;
    }


    public LinkedList<MoveMessage> getMoveMessages(){
        return moveMessages;
    }

    public void setMoveMessages(LinkedList<MoveMessage> moveStrings){
        this.moveMessages = moveStrings;
    }

    /**
     * Calls the set callback to write a message and saves it to moveMessages.
     * @param moveMessage
     */
    public void postMessage(String moveMessage){
        postMessage(new MoveMessage(moveMessage, MoveMessageType.NEUTRAL));
    }

    /**
     * Calls the set callback to write a move and saves it to moveMessages.
     * @param moveMessage move string
     * @param isWhite weather the move is white. Used for the display
     */
    public void postMessage(String moveMessage, boolean isWhite){
        postMessage(new MoveMessage(moveMessage, isWhite? MoveMessageType.WHITE : MoveMessageType.BLACK));
    }

    private void postMessage(MoveMessage message){
        moveMessages.add(message);
        if(messageCallback != null){
            messageCallback.run(message.content, message.type);
        }
    }
}
