package cz.cvut.fel.buninmat.pjv.mychess.view.board;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Figure;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Abstract board control. Draws the board and provides helper functions.
 */
public abstract class BoardUI extends VBox {

    private final double notationThickness = 1;
    private double fieldSize;
    private double figureSize;

    GridPane mainGrid;

    BoardModel boardModel;

    BoardUI(int width, int height, BoardModel boardModel){
        this.boardModel = boardModel;

        mainGrid = new GridPane();
        mainGrid.getStylesheets().add("styles/board.css");

        double size = width < height? width : height;
        fieldSize = (size - notationThickness) / 8;
        figureSize = fieldSize * 0.60;

        mainGrid.setMinSize(size, size);

        initBoard();

        setPrefHeight(height);
        setPrefWidth(width);
        setSpacing(5);
    }

    double getFieldSize(){
        return fieldSize;
    }

    double getFigureSize(){
        return figureSize;
    }

    private void initBoard(){

        for(int i = 0; i < 9; i++){
            for(int j = 0; j < 9; j++){
                if(i == 8 || j == 0){
                    if(i != 8 || j != 0) {
                        addNotationField(i, j);
                    }
                    continue;
                }
                addField(i, j);
                addFigure(i, j);
            }
        }
    }

    private void addField(int i, int j){
        Button field = new Button();
        field.setPrefSize(fieldSize, fieldSize);
        field.getStyleClass().add(getDefaultStyle(i, j));
        field.setId(id(i, j));
        addFieldHandlers(field, i, j);
        mainGrid.add(field, j, i);
    }

    private String id(int i, int j){
        return String.format("%d%d", j - 1, i);
    }

    private void addNotationField(int i, int j){
        Label lbl = new Label();
        lbl.setAlignment(javafx.geometry.Pos.BASELINE_CENTER);
        lbl.getStyleClass().add("notation-strip");

        if (j == 0) {
            int num = boardModel.opponentWhite()? i + 1 : 8 - i;
            lbl.setText(num + " ");
            lbl.setPrefSize(notationThickness, fieldSize);
        } else {
            char letter = (char)(boardModel.opponentWhite()? ('h' - j + 1) : ('a' + j - 1));
            lbl.setText(String.valueOf(letter));
            lbl.setPrefSize(fieldSize, notationThickness);
        }

        mainGrid.add(lbl, j, i);
    }

    void addFigure(int i, int j){
        Figure figure = boardModel.board.getFigure(toBoardPos(i, j));
        if(figure == null) {
            return;
        }
        String url = figure.isWhite ? figure.getType().whiteImageUrl : figure.getType().blackImageUrl;
        ImageView image = new ImageView(new Image(url));
        image.setFitHeight(getFigureSize());
        image.setFitWidth(getFigureSize());
        getField(i, j).setGraphic(image);
    }

    void addFieldHandlers(Node field, int i, int j){

    }

    Button getField(Pos pos){
        return getField(pos.x, pos.y);
    }

    Button getField(int i, int j){
        return (Button)mainGrid.lookup("#" + id(i, j));
    }

    Pos toBoardPos(int i, int j){
        return boardModel.opponentWhite()? new Pos(8 - j, i): new Pos(j - 1, 7 - i);
    }

    Pos fromBoardPos(Pos pos){
        return boardModel.opponentWhite()? new Pos(pos.y, 8 - pos.x): new Pos( 7 - pos.y, pos.x + 1);
    }

    static String getActiveStyle(int i, int j){
        return isWhite(i, j)? "field-white-active" : "field-black-active";
    }

    static String getCheckStyle(int i, int j){
        return isWhite(i, j)? "field-white-checked": "field-black-checked";
    }

    static String getDefaultStyle(int i, int j){
        return isWhite(i, j)? "field-white": "field-black";
    }

    private static boolean isWhite(int i, int j){
        return (i + j) % 2 != 0;
    }
}
