package cz.cvut.fel.buninmat.pjv.mychess.engine;

import java.util.LinkedList;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

/**
 * Abstract class for describing figure behaviour.
 */
public abstract class Figure {

    public final Boolean isWhite;

    public abstract FigureType getType();

    /**
     * Constructs figure of specified color
     * @param white color of figure
     */
    Figure(Boolean white){
        this.isWhite = white;
    }

    /**
     * Finds moves that can be made by some figure on board
     * @param board that figure is located on
     * @param from position of the figure on the board
     * @return list of moves that a figure can do on the board
     */
    public abstract LinkedList<Move> getMoves(Board board, Pos from);

    /**
     * Finds fields that are attacked by a figure
     * @param from location of the figure on board
     * @param board board that figure is located on
     * @return list of fields that are attacked by the figure
     */
    public abstract LinkedList<Pos> getAttacks(Pos from, Board board);

    LinkedList<Pos> attacksFromDirs(Pos from, Pos[] dirs){
        LinkedList<Pos> ret = new LinkedList<>();
        for(Pos dir : dirs){
            Pos attack = from.plus(dir);
            if(!attack.nonBoard()){
                ret.add(attack);
            }
        }
        return ret;
    }

    LinkedList<Pos> attacksLineFromDirs(Pos from, Pos[] dirs, Board board){
        LinkedList<Pos> res =  new LinkedList<>();
        Figure figure = board.getFigure(from);
        for(Pos dir : dirs){
            Pos cur = from.plus(dir);
            while(! cur.nonBoard()){
                Figure target = board.getFigure(cur);
                if(target != null){
                    if(target.isWhite != figure.isWhite){
                        res.add(cur);
                    }
                    break;
                }
                res.add(cur);
                cur = cur.plus(dir);
            }
        }
        return res;
    }

    LinkedList<Move> movesFromAttacksLine(Pos from, Board board){
        LinkedList<Move> moves = new LinkedList<>();
        for(Pos to : getAttacks(from, board)){
            moves.add(Move.regular(getType(), from, to));
        }
        return moves;
    }
}
