package cz.cvut.fel.buninmat.pjv.mychess.model;

import cz.cvut.fel.buninmat.dbs.chessdb.DBManager;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Game;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;
import cz.cvut.fel.buninmat.dbs.chessdb.model.types.GameResult;
import cz.cvut.fel.buninmat.pjv.mychess.Main;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Board;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Move;
import cz.cvut.fel.buninmat.pjv.mychess.opponent.Opponent;

/**
 * Manages game state and logic
 */
public class BoardModel {

    public Board board;

    private ClocksModel blackClocks, whiteClocks;
    private boolean clocks;
    private Runnable outOfTimeCallback;

    private WriterModel writerModel;

    private Opponent opponent;

    private Move lastMove;

    private boolean opponentWhite;

    private Player opponentUser;

    private boolean writeDb;

    private Game game;

    private int moveNum = 0;

    public boolean opponentWhite(){
        return opponentWhite;
    }

    public void setOpponentWhite(boolean value){
        opponentWhite = value;
    }

    public BoardModel(Board board) {
        this.board = board;
    }

    /**
     * Assumes the default board
     */
    public BoardModel(){
        board = new Board();
    }

    /**
     * Add clocks for black and white players
     * @param blackClocks
     * @param whiteClocks
     * @return the instance
     */
    public BoardModel addClocks(ClocksModel blackClocks, ClocksModel whiteClocks){
        this.blackClocks = blackClocks;
        this.whiteClocks = whiteClocks;
        blackClocks.setOnFinished(this::onTimeFinished);
        whiteClocks.setOnFinished(this::onTimeFinished);
        clocks = true;
        return this;
    }

    /**
     * Add WriterModel to write moves to
     * @param writerModel
     * @return the instance
     */
    public BoardModel addWriter(WriterModel writerModel){
        this.writerModel = writerModel;
        return this;
    }

    /**
     * Opponent to ask for moves
     * @param opponent
     * @return the instance
     */
    public BoardModel addOpponent(Opponent opponent){
        this.opponent = opponent;
        return this;
    }

    /**
     * Takes a random "opponent" player from the database and adds it as opponentUser
     * @return the instance
     */
    public BoardModel addOpponentUser() {
        if(DBManager.getInstatnce().isSetup()) {
            opponentUser = DBManager.getInstatnce().randomPlayer();
        }
        return this;
    }

    /**
     * Create corresponding game entity in database to write moves afterwards.
     */
    public void initDbWriting(){
        writeDb = true;

        //add game with players to database

        game = new Game(false, true, GameResult.NONE);
        DBManager.getInstatnce().submitGame(game);

        //write white player as the first and black player as the second

        Player white, black;
        if(opponentWhite) {
            white = opponentUser;
            black = Main.getInstance().getUser();
        }else{
            white = Main.getInstance().getUser();
            black = opponentUser;
        }
        DBManager.getInstatnce().addPlayersPair(game, white, black);
    }

    /**
     *
     * @return boardModel that an opponent should use. Used for network game.
     */
    public BoardModel getOpponentBoardModel(){
        BoardModel oppBoardModel = new BoardModel(board);
        oppBoardModel.opponentWhite = !opponentWhite;
        oppBoardModel.lastMove = lastMove;
        oppBoardModel.whiteClocks = whiteClocks;
        oppBoardModel.blackClocks = blackClocks;
        return oppBoardModel;
    }

    public ClocksModel getBlackClocks(){
        return blackClocks;
    }

    public ClocksModel getWhiteClocks(){
        return whiteClocks;
    }

    public boolean hasClocks(){
        return clocks;
    }

    public WriterModel getWriterModel(){
        return writerModel;
    }

    public Opponent getOpponent(){return opponent; }

    public Game getGame() {
        return game;
    }

    public void setOutOfTimeCallback(Runnable runnable){
        outOfTimeCallback = runnable;
    }

    public Move getLastMove() {
        return lastMove;
    }

    //A random user that you "play with"
    public Player getOpponentUser() {
        return opponentUser;
    }

    /**
     * Makes the move, performing all the game state changes necessary
     * @param move move to make
     */
    public void makeMove(Move move){
        moveNum++;
        move.make(board);
        board.switchSides();
        board.setCheckPos(board.getCheck());
        board.switchSides();
        board.setStall(!board.hasMove());
        if(clocks){
            //switch clocks after sides are switched
            ClocksModel clocks, opponentClocks;
            if(board.isWhiteTurn()){
                clocks = whiteClocks;
                opponentClocks = blackClocks;
            }else {
                clocks = blackClocks;
                opponentClocks = whiteClocks;
            }
            if(!clocks.isRunning() && ! board.isStall()){
                clocks.start();
            }
            //used for synchronising clocks in notwork game
            move.setTimestamp(opponentClocks.toString());

            opponentClocks.stop();
        }
        boolean draw = board.isStall() && !board.isMate();
        if(writerModel != null){
            String message = move.toString();
            if(board.isMate()){
                message += "#";
            }
            else if(board.isCheck()){
                message += "+";
            }
            writerModel.postMessage(message, board.isWhiteTurn());
            if(draw){
                writerModel.postMessage("draw");
            }
        }
        //send move to database
        if(writeDb){
            DBManager.getInstatnce().submitMove(game, move, moveNum);
            if(board.isMate()){
                game.setResult(board.isWhiteTurn()? GameResult.BLACK_WIN : GameResult.WHITE_WIN);
            }else if(draw){
                game.setResult(GameResult.DRAW);
            }
            DBManager.getInstatnce().submitGame(game);
        }
        lastMove = move;
    }

    private void onTimeFinished(){
        if(outOfTimeCallback != null){
            outOfTimeCallback.run();
        }
        if(writerModel != null){
            writerModel.postMessage(String.format("%s win (time is over)", board.isWhiteTurn() ? "black" : "white"));
        }
        whiteClocks.stop();
        blackClocks.stop();
    }

    public void pauseClocks(){
        if(clocks) {
            whiteClocks.stop();
            blackClocks.stop();
        }
    }

    public void resumeClocks(){
        if(clocks) {
            whiteClocks.start();
            blackClocks.start();
        }
    }
}