package cz.cvut.fel.buninmat.pjv.mychess.view.board;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Figure;
import cz.cvut.fel.buninmat.pjv.mychess.engine.FigureType;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;

/**
 * Board control for placing figures manually
 */
public class DragBoardUI extends BoardUI {

    private Figure figureDragged;

    public DragBoardUI(int width, int height, BoardModel boardModel) {
        super(width, height, boardModel);
        getChildren().add(mainGrid);
        getChildren().add(getFigureSetPane(false));
        getChildren().add(getFigureSetPane(true));
    }

    private HBox getFigureSetPane(boolean isWhite){
        HBox hBox = new HBox();
        for(FigureType figureType : FigureType.values()){
            Image image = new Image(isWhite ? figureType.whiteImageUrl : figureType.blackImageUrl);
            ImageView figureImage = new ImageView(image);
            figureImage.setFitWidth(50); figureImage.setFitHeight(50);
            figureImage.addEventHandler(MouseEvent.DRAG_DETECTED,
                    event ->{
                figureDragged = figureType.makeInstance(isWhite);
                startDrag(event, figureImage, image);
            });
            hBox.getChildren().add(figureImage);
        }
        return hBox;
    }

    private void startDrag(MouseEvent event, Node node, Image image){
        Dragboard db = node.startDragAndDrop(TransferMode.MOVE);
        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putImage(image);
        db.setContent(clipboardContent);
        event.consume();
    }

    @Override
    void addFieldHandlers(Node field, int i, int j){
        Pos pos = toBoardPos(i, j);
        field.addEventHandler(MouseEvent.DRAG_DETECTED,
                event -> {
                    figureDragged = boardModel.board.getFigure(pos);
                    if(figureDragged != null) {
                        startDrag(event, field,
                                new Image(figureDragged.isWhite ?
                                        figureDragged.getType().whiteImageUrl :
                                        figureDragged.getType().blackImageUrl));
                        boardModel.board.setFigure(pos, null);
                        getField(i, j).setGraphic(null);
                    }
                });

        field.addEventHandler(DragEvent.DRAG_DROPPED, event -> {
            if(figureDragged != null) {
                boardModel.board.setFigure(pos, figureDragged);
                addFigure(i, j);
                field.getStyleClass().remove(1);
                field.getStyleClass().add(getDefaultStyle(i, j));
            }
            figureDragged = null;
        });
        field.addEventHandler(DragEvent.DRAG_ENTERED, event -> {
            if(figureDragged != null){
                field.getStyleClass().remove(1);
                field.getStyleClass().add(getActiveStyle(i, j));
            }
        });
        field.addEventHandler(DragEvent.DRAG_OVER, event -> {
            if(figureDragged != null) {
                event.acceptTransferModes(TransferMode.ANY);
            }
            event.consume();
        });
        field.addEventHandler(DragEvent.DRAG_EXITED, event -> {
            if(figureDragged != null){
                field.getStyleClass().remove(1);
                field.getStyleClass().add(getDefaultStyle(i, j));
            }
        });
    }
}
