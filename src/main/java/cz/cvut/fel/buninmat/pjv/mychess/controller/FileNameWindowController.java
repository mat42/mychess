package cz.cvut.fel.buninmat.pjv.mychess.controller;

import cz.cvut.fel.buninmat.pjv.mychess.game_saving.GameRecord;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 * Controller for FileNameWindow.fxml
 */
public class FileNameWindowController {

    @FXML
    private GridPane mainGrid;
    @FXML
    private TextField fileNameField;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    public interface FinishedCallback{
        void run(String filename);
    }

    private FinishedCallback onFinished;

    /**
     * Must be set before initialization
     * @param callback to be called once file input is complete
     */
    public void setOnFinished(FinishedCallback callback){
        onFinished = callback;
    }

    public void cancel(){
        if(onFinished != null){
            onFinished.run("");
        }
    }

    @FXML
    private void initialize(){
        mainGrid.getStylesheets().add("styles/common.css");
        mainGrid.getStylesheets().add("styles/game-create.css");
        String defaultFilename = GameRecord.getNextDefaultFileName();
        fileNameField.setText(defaultFilename);
        okButton.setOnMouseClicked(event->{
            if(onFinished == null){
                return;
            }
            String text = fileNameField.getText();
            onFinished.run(isValidFilename(text) ? text : defaultFilename);
            getStage().close();
        });
        cancelButton.setOnMouseClicked(event->{
            cancel();
            getStage().close();
        });
    }

    private Stage getStage(){
        return (Stage) mainGrid.getScene().getWindow();
    }

    private boolean isValidFilename(String str){
        try{
            Paths.get(str);
        }catch (InvalidPathException e){
            return false;
        }
        return true;
    }
}
