package cz.cvut.fel.buninmat.pjv.mychess.controller;

import cz.cvut.fel.buninmat.dbs.chessdb.DBManager;
import cz.cvut.fel.buninmat.dbs.chessdb.model.GameMoves;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Opponents;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;
import cz.cvut.fel.buninmat.pjv.mychess.model.WriterModel;
import cz.cvut.fel.buninmat.pjv.mychess.view.MovesPane;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * Controller for ProfileWindow.fxml
 */
public class ProfileController {
    @FXML
    private GridPane mainGrid;

    @FXML
    private Label usernameLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private VBox gamesList;

    private Player player;

    private MovesPane movesPane = new MovesPane();

    /**
     * Must be set before initialisation
     * @param player identifies player profile to be displayed
     */
    public void setPlayer(Player player) {
        this.player = player;
        usernameLabel.setText(player.getNick());
        emailLabel.setText(player.getEmail());
        initGamesList();
        movesPane.setPrefHeight(400);
        movesPane.setPrefWidth(200);
        mainGrid.add(movesPane, 1, 1);
    }

    @FXML
    private void initialize(){
        mainGrid.getStylesheets().add("styles/common.css");
        mainGrid.getStylesheets().add("styles/game.css");
    }

    private void initGamesList(){
        List<Opponents> opponents = DBManager.getInstatnce().getOpponents(player);
        for(Opponents opp : opponents){
            gamesList.getChildren().add(new GameListItem(opp, this::showMoves));
        }
    }

    private void showMoves(List<GameMoves> moves){
        movesPane.clear();
        boolean white = false;
        for(GameMoves move : moves){
            movesPane.addMoveLabel(move.getGameMoveId().getMoveStr(),
                    white ? WriterModel.MoveMessageType.WHITE : WriterModel.MoveMessageType.BLACK);
            white = !white;
        }
    }
}

