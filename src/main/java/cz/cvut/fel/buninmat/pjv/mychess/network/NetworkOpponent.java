package cz.cvut.fel.buninmat.pjv.mychess.network;

import cz.cvut.fel.buninmat.pjv.mychess.game_saving.GameSaver;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import javafx.application.Platform;
import javafx.concurrent.Task;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Move;
import cz.cvut.fel.buninmat.pjv.mychess.opponent.Opponent;

import java.io.*;
import java.net.Socket;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * Manages a game on the created socket
 */
public class NetworkOpponent implements Opponent {

    private Socket gameSocket;

    private boolean active;

    private BufferedReader in;
    private BufferedWriter out;

    private Thread socketThread;

    private static final Logger LOG = Logger.getLogger(Communicator.class.getName());

    NetworkOpponent(Socket gameSocket){
        this.gameSocket = gameSocket;
        try {
            in = new BufferedReader(new InputStreamReader(gameSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(gameSocket.getOutputStream()));
        }catch (IOException e) {
            LOG.severe("Failed to create socket stream");
        }
        active = true;
    }

    /**
     * Stop the communication and close socket
     */
    public void terminate(){
        joinSocketThread();
        LOG.info("Game finished");
        try {
            gameSocket.close();
        }catch (IOException e){
            LOG.severe("Error while closing socket");
        }
        active = false;
    }

    public boolean isActive(){
        return active;
    }

    /**
     * Get the BoardModel to play with
     * @param consumer gets BoardModel
     */
    public void getGameAsync(Consumer<BoardModel> consumer){
        runSocketTask(new Task() {
            @Override
            protected Object call() throws Exception {

                BoardModel bm = GameSaver.readFromBuffer(in);
                if(bm == null){
                    terminate();
                }else {
                    bm.addOpponent(NetworkOpponent.this);
                    Platform.runLater(() -> consumer.accept(bm));
                }

                return null;
            }
        });
    }

    /**
     * Sends the boardModel for the opponent to use
     */
    public void sendGameAsync(BoardModel boardModel){
        runSocketTask(new Task() {
            @Override
            protected Object call() throws Exception {
                GameSaver.writeToBuffer(out, boardModel.getOpponentBoardModel());
                out.flush();
                return null;
            }
        });
    }

    private void runSocketTask(Task task){
        joinSocketThread();
        socketThread = new Thread(task);
        socketThread.setDaemon(true);
        socketThread.start();
    }

    private void joinSocketThread(){
        if(socketThread != null){
            try {
                socketThread.join();
            }catch (InterruptedException e){
                Thread.currentThread().interrupt();
            }
        }
        socketThread = null;
    }

    @Override
    public Move getMove(BoardModel boardModel) {
        return null;
    }

    /**
     * Send the move made, then wait for move from the opponent and then call onMove
     * @param onMove gets the move
     */
    @Override
    public void getMoveAsync(BoardModel boardModel, OnMoveAction onMove) {
        runSocketTask(new Task() {
            @Override
            protected Object call() throws Exception {
                try{
                    Move move =  boardModel.getLastMove();
                    if(move != null) {
                        //send last move
                        String moveStr = String.format("%s %s", move.toString(), move.getTimestamp());
                        out.write(moveStr);
                        out.newLine();
                        out.flush();
                        LOG.info(String.format("sent %s", moveStr));
                    }
                    //get move in response
                    LOG.info("reading move");
                    String resp = in.readLine();
                    LOG.info(String.format("received %s", resp));

                    if(resp.equals("end")){
                        terminate();
                        return null;
                    }
                    String[] tokens = resp.split(" ");
                    Move oppMove = Move.parseMove(tokens[0], boardModel.board.isWhiteTurn());

                    Platform.runLater(()->{
                        onMove.run(oppMove);
                    });

                    //TODO adjust boardModel clocks to
                }catch (IOException e){
                    LOG.severe(e.getMessage());
                }
                return null;
            }
        });
    }
}
