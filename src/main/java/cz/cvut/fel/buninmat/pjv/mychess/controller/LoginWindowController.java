package cz.cvut.fel.buninmat.pjv.mychess.controller;

import cz.cvut.fel.buninmat.dbs.chessdb.DBManager;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import java.util.function.Consumer;

/**
 * Controller for LoginWindow.fxml
 */
public class LoginWindowController {

    @FXML
    private GridPane mainGrid;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button loginButton;
    @FXML
    private Label messageLabel;

    private Consumer<Player> consumer;

    /**
     * Called when login is successful to pass the Player object
     * @param consumer receives player
     */
    public void setPlayerConsumer(Consumer<Player> consumer){
        this.consumer = consumer;
    }

    @FXML
    private void initialize(){
        mainGrid.getStylesheets().add("styles/common.css");
        mainGrid.getStylesheets().add("styles/game-create.css");

        loginButton.setOnMouseClicked(event->{
            if(consumer == null){
                return;
            }
            String text = usernameField.getText();
            String password = passwordField.getText();
            if(password.isEmpty() || text.isEmpty()){
                messageLabel.setVisible(true);
                messageLabel.setText("Something is missing");
                return;
            }
            if(!DBManager.getInstatnce().isSetup()){
                messageLabel.setVisible(true);
                messageLabel.setText("No DB connection");
            }
            Player player = DBManager.getInstatnce().authenticate(text, password.hashCode());
            if(player == null){
                messageLabel.setVisible(true);
                messageLabel.setText("Authentication failed");
                return;
            }
            consumer.accept(player);
            getStage().close();
        });
    }

    private Stage getStage(){
        return (Stage) mainGrid.getScene().getWindow();
    }
}
