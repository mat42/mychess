package cz.cvut.fel.buninmat.pjv.mychess.network;

import javafx.application.Platform;
import javafx.concurrent.Task;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;

/**
 * Manages client-server creation of a socket for a network game
 */
public class Communicator {

    private static final Logger LOG = Logger.getLogger(Communicator.class.getName());

    private final int serverPort = 6666;

    private Thread serverThread;
    private Task waitTask;

    public interface OnConnectedCallback{
        void run(NetworkOpponent opponent);

    }

    private static Communicator instance = new Communicator();

    public static Communicator getInstance(){
        return instance;
    }

    private Communicator(){}

    /**
     * Connects to a server opponent
     * @param onConnected receives the new NetworkOpponent
     */
    public void connectToOpponent(OnConnectedCallback onConnected){
        Task connectTask = new Task() {
            @Override
            protected Object call() throws Exception {
                try {
                    Socket clientSocket = new Socket("localhost", serverPort);
                    //Create an opponent when connection is established
                    Platform.runLater(()->onConnected.run(new NetworkOpponent(clientSocket)));
                }
                catch (UnknownHostException e){
                    LOG.severe("Unknown host");
                }
                catch (IOException e){
                    LOG.severe("Failed to connect the host");
                }
                return null;
            }
        };
        Thread clientThread = new Thread(connectTask);
        clientThread.setDaemon(true);
        clientThread.start();
    }

    /**
     * Waits on server port until an opponent client connects
     * @param onConnected receives the new NetworkOpponent
     */
    public void waitForOpponent(OnConnectedCallback onConnected){
        cancelWait();
        waitTask = new Task() {
            @Override
            protected Object call() throws Exception {
                ServerSocket serverSocket;
                try{
                    serverSocket = new ServerSocket(serverPort);
                }catch (IOException e){
                    LOG.severe(String.format("Failure while initialising server: port %d is blocked", serverPort));
                    cancel();
                    return null;
                }
                while (!isCancelled()) {
                    Socket clientSocket;
                    try {
                        clientSocket = serverSocket.accept();
                    } catch (IOException e) {
                        LOG.severe("Server connection failure");
                        cancel();
                        return null;
                    }

                    //Create an opponent when connection is established
                    Platform.runLater(()->onConnected.run(new NetworkOpponent(clientSocket)));
                }
                return null;
            }
        };
        serverThread = new Thread(waitTask);
        serverThread.setDaemon(true);
        serverThread.start();
    }

    private void cancelWait(){
        if(serverThread != null && serverThread.isAlive()){
            waitTask.cancel();
            try {
                serverThread.join();
            }catch (InterruptedException e){
                Thread.currentThread().interrupt();
            }
        }
    }
}
