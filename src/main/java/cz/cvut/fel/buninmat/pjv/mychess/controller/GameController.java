package cz.cvut.fel.buninmat.pjv.mychess.controller;

import cz.cvut.fel.buninmat.dbs.chessdb.DBManager;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Game;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;
import cz.cvut.fel.buninmat.pjv.mychess.view.MovesPane;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import cz.cvut.fel.buninmat.pjv.mychess.Main;
import cz.cvut.fel.buninmat.pjv.mychess.game_saving.GameSaver;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import cz.cvut.fel.buninmat.pjv.mychess.model.ClocksModel;
import cz.cvut.fel.buninmat.pjv.mychess.model.WriterModel;
import cz.cvut.fel.buninmat.pjv.mychess.view.board.GameBoardUI;

/**
 * Control for GameWindow.fxml (board, clocks and panel where moves are displayed)
 */
public class GameController {

    @FXML
    private GridPane mainGrid;

    @FXML
    private GridPane boardGrid;

    @FXML
    private Label blackClocks;

    @FXML
    private Label whiteClocks;

    @FXML
    private MovesPane textPane;

    @FXML
    private Button saveGameButton;

    @FXML
    private Label userLabel;

    @FXML
    private Label opponentUserLabel;

    private BoardModel boardModel;

    /**
     * BoardModel setter, must be set before initialization
     * @param boardModel board model representing game that is running
     */
    public void setBoardModel(BoardModel boardModel){
        this.boardModel = boardModel;
        setupGame();
    }

    private void setupGame(){
        if(boardModel.getWhiteClocks() != null && boardModel.getBlackClocks() != null) {
            boardModel.getBlackClocks().setTargetTimeString(blackClocks.textProperty());
            blackClocks.textProperty().addListener((observable, oldValue, newValue) ->
                    updateClocksStyle(boardModel.getBlackClocks(), blackClocks));

            boardModel.getWhiteClocks().setTargetTimeString(whiteClocks.textProperty());
            whiteClocks.textProperty().addListener((observable, oldValue, newValue) ->
                    updateClocksStyle(boardModel.getWhiteClocks(), whiteClocks));
        }

        WriterModel writerModel = boardModel.getWriterModel();
        if(writerModel != null) {
            writerModel.setMessageCallback((c, t)->textPane.addMoveLabel(c, t));
        }
        boardGrid.add(new GameBoardUI(400, 600, boardModel), 0, 0, 1, 3);

        ImageView saveIcon = new ImageView("icons/save-icon.png");
        saveIcon.setFitWidth(20);
        saveIcon.setFitHeight(25);
        saveGameButton.setGraphic(saveIcon);

        for(WriterModel.MoveMessage move : boardModel.getWriterModel().getMoveMessages()){
            textPane.addMoveLabel(move.content, move.type);
        }
        addUserLabels();
    }

    private void addUserLabels(){
        Player user = Main.getInstance().getUser();
        if(user != null){
            userLabel.setText(user.getNick());
        }
        Player oppUser = boardModel.getOpponentUser();
        if(oppUser != null){
            opponentUserLabel.setText(oppUser.getNick());
        }
    }

    @FXML
    private void initialize(){
        mainGrid.getStylesheets().add("styles/common.css");
        mainGrid.getStylesheets().add("styles/game.css");
        saveGameButton.setOnMouseClicked(event -> onSaveGameButton());
    }

    /**
     * Writes to database that game is finished and closes sockets.
     * Is supposed to be bound to close event
     */
    public void onClose(){
        //set Game.active to false in database
        Game game = boardModel.getGame();
        if(game == null){
            return;
        }
        game.setActive(false);
        DBManager.getInstatnce().submitGame(game);
    }

    private void onSaveGameButton(){
        boardModel.pauseClocks();
        Main.getInstance().runFilenameDialog((Stage)mainGrid.getScene().getWindow(),
                filename -> {
                    if(!filename.equals("")) {
                        GameSaver.save(filename, boardModel);
                    }
                    boardModel.resumeClocks();
                });
    }

    private void updateClocksStyle(ClocksModel clocksModel, Label clocks){
        final int littleTimeDecSec = 300;
        if(clocksModel.getTimeDecSec() <= littleTimeDecSec){
            clocks.getStyleClass().remove(1);
            clocks.getStyleClass().add("little-time-clocks-label");
        }
    }
}
