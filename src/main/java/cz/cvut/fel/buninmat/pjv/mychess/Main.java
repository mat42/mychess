package cz.cvut.fel.buninmat.pjv.mychess;

import cz.cvut.fel.buninmat.dbs.chessdb.DBManager;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;
import cz.cvut.fel.buninmat.pjv.mychess.controller.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import cz.cvut.fel.buninmat.pjv.mychess.opponent.Opponent;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Provides start method and methods for launching windows and loading controls
 */
public class Main extends Application {

    private static Main currentMain;

    private Player user;

    public Player getUser() {
        return user;
    }

    public void setUser(Player user) {
        this.user = user;
    }

    /**
     * Launches start window and calls database module initialisation
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        currentMain = this;
        showMainWindow();
        //setup database connection
        DBManager.getInstatnce().setup();
        DBManager.getInstatnce().init();
    }

    @Override
    public void stop(){
        //close database connection
        DBManager.getInstatnce().close();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static Main getInstance() {
        return currentMain;
    }

    private void showMainWindow(){
        try {
            FXMLLoader loader = getLoader("MainWindow");
            showWindow(loader.load(), 600, 400);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showProfileWindow(Player player){
        try {
            FXMLLoader loader = getLoader("ProfileWindow");
            Parent parent = loader.load();

            ProfileController controller = loader.getController();
            controller.setPlayer(player);

            showWindow(parent, 400, 400);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showCreateGameWindow(){
        showCreateGameWindow(null);
    }

    private final int gameWindowWidth = 850, gameWindowHeight = 600;

    public void showCreateGameWindow(Opponent opponent){
        try {
            FXMLLoader loader = getLoader("GameCreateWindow");
            Parent parent = loader.load();
            if(opponent != null){
                GameCreateController controller = loader.getController();
                controller.setOpponent(opponent);
            }
            showWindow(parent, gameWindowWidth, gameWindowHeight);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showGameWindow(BoardModel boardModel){
        try {
            FXMLLoader loader = getLoader("GameWindow");
            Parent parent = loader.load();
            GameController gameController = loader.getController();
            gameController.setBoardModel(boardModel);

            Stage stage = getWindowStage(parent, gameWindowWidth, gameWindowHeight);
            //bind window on close event
            stage.setOnCloseRequest(event -> gameController.onClose());
            stage.show();

        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public void loadGameControl(Stage stage, BoardModel boardModel){

        try {
            FXMLLoader loader = getLoader("GameWindow");
            Parent parent = loader.load();
            GameController gameController = loader.getController();
            gameController.setBoardModel(boardModel);

            //bind window on close event
            stage.setOnCloseRequest(event -> gameController.onClose());

            stage.setScene(new Scene(parent));

        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public void runFilenameDialog(Stage parentStage, FileNameWindowController.FinishedCallback onFinished){
        FXMLLoader loader = getLoader("FileNameWindow");

        Stage stage = getDialogStage(parentStage, loader);

        FileNameWindowController controller = loader.getController();
        controller.setOnFinished(onFinished);

        stage.setOnCloseRequest(event -> controller.cancel());

        stage.show();
    }

    public void showLoginDialog(Stage parentStage, Consumer<Player> playerConsumer){
        FXMLLoader loader = getLoader("LoginWindow");

        Stage stage = getDialogStage(parentStage, loader);


        LoginWindowController controller = loader.getController();
        controller.setPlayerConsumer(playerConsumer);

        stage.show();
    }

    private Stage getDialogStage(Stage parentStage, FXMLLoader loader){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        int width = 200, height = 150;
        stage.setMinWidth(width);
        stage.setMinHeight(height);
        stage.setMaxWidth(width);
        stage.setMaxHeight(height);
        try {
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            stage.setX(parentStage.getX() + (parentStage.getWidth() - width) / 2);
            stage.setY(parentStage.getY() + (parentStage.getHeight() - height) / 2);
        }catch (IOException e){
            throw new RuntimeException(e);
        }
        return stage;
    }

    private FXMLLoader getLoader(String name) {
        return new FXMLLoader(getClass().getResource(String.format("/fxml/%s.fxml", name)));
    }

    private void showWindow(Parent parent, int minWidth, int minHeight){
        getWindowStage(parent, minWidth, minHeight).show();
    }

    private Stage getWindowStage(Parent parent, int minWidth, int minHeight){
        Stage stage = new Stage();
        stage.setTitle("My Chess");
        stage.getIcons().add(new Image("icons/knight_w.png"));
        stage.setMinWidth(minWidth);
        stage.setWidth(minWidth);
        stage.setMinHeight(minHeight);
        stage.setHeight(minHeight);

        Scene scene = new Scene(parent);
        stage.setScene(scene);

        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);

        return stage;
    }

}
