package cz.cvut.fel.buninmat.pjv.mychess.game_saving;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;

/**
 * Represents a saved game
 */
public class GameRecord {

    private String name;

    private static final String baseDir = "metadata";

    static String getGamesDirectory(){
        return baseDir;
    }

    /**
     *
     * @param name name of new record (file name)
     */
    GameRecord(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    String getUrl() {
        return baseDir + File.separator + name + ".mypgn";
    }

    /**
     * List of all game records saved
     * @return all the records in the directory with saved games
     */
    public static LinkedList<GameRecord> listRecords(){
        LinkedList<GameRecord> gameRecords = new LinkedList<>();
        try(DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(baseDir))){
            for(Path path : stream){
                String name = path.getFileName().toString();
                name = name.substring(0, name.lastIndexOf("."));
                gameRecords.add(new GameRecord(name));
            }
        }catch (IOException ignored){} //the directory does not exist in the beginning
        return gameRecords;
    }

    /**
     * Make a new default name with numbering according to how many files with the same name are already there
     * @return file name
     */
    public static String getNextDefaultFileName(){
        String name = "GameSaver";
        int idx = 0;
        for(GameRecord record : listRecords()){
            String[] parts = record.name.split("_");
            if(parts[0].equals(name)){
                if(parts.length == 1){
                    idx++;
                    continue;
                }
                int num = idx;
                try {
                    num = Integer.parseInt(parts[1]);
                } catch (NumberFormatException ignored) {
                }
                if (num > idx) {
                    idx = num;
                }
            }
        }
        if(idx != 0){
            name = String.format("%s_%d", name, idx);
        }
        return name;
    }
}
