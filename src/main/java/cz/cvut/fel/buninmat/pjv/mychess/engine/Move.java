package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

/**
 * Represents a move to be made.
 */
public class Move {
    private Pos from, to;
    private FigureType figureType;
    //castle
    private Pos rookFrom, rookTo;
    //en passant
    private Pos enPassantTarget;
    private Pos poneJumpPos;

    private boolean poneTransformation;
    private FigureType poneTransformationFigure = FigureType.QUEEN;

    private boolean castle, enPassant;

    private String timestamp;

    private boolean chooseTransformation;

    public Pos getFrom() {
        return from;
    }

    public Pos getTo() {
        return to;
    }

    public Pos getRookFrom() {
        return rookFrom;
    }

    public Pos getRookTo() {
        return rookTo;
    }

    public Pos getEnPassantTarget() {
        return enPassantTarget;
    }

    public boolean isCastle() {
        return castle;
    }

    public boolean isEnPassant() {
        return enPassant;
    }

    public boolean isPoneTransformation() {
        return poneTransformation;
    }

    public void setPoneTransformationFigure(FigureType figure) {
        poneTransformationFigure = figure;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean chooseTransformation() {
        return chooseTransformation;
    }

    public void setChooseTransformation(boolean chooseTransformation) {
        this.chooseTransformation = chooseTransformation;
    }

    private Move(FigureType figureType, Pos from, Pos to) {
        this.figureType = figureType;
        this.from = from;
        this.to = to;
    }

    /**
     * Regular move of a figure (including a takes)
     * @param figureType
     * @param from
     * @param to
     * @return move instance
     */
    static Move regular(FigureType figureType, Pos from, Pos to) {
        return new Move(figureType, from, to);
    }

    /**
     * Castle move, which is a move of the King. Change of the Rook position should also be specified
     * @param from
     * @param to
     * @param rookFrom
     * @param rookTo
     * @return
     */
    static Move castle(Pos from, Pos to, Pos rookFrom, Pos rookTo) {
        Move move = new Move(FigureType.KING, from, to);
        move.rookFrom = rookFrom;
        move.rookTo = rookTo;
        move.castle = true;
        return move;
    }

    /**
     * First move of the pone over two fields
     * @param from
     * @param to
     * @param jumpPos position that pone passed during the move (needed for en passant)
     * @return
     */
    static Move poneJump(Pos from, Pos to, Pos jumpPos) {
        Move move = new Move(FigureType.PONE, from, to);
        move.poneJumpPos = jumpPos;
        return move;
    }

    /**
     * En passant
     * @param from
     * @param to
     * @param enPassantTarget position of the taken pone
     * @return
     */
    static Move enPassant(Pos from, Pos to, Pos enPassantTarget) {
        Move move = new Move(FigureType.PONE, from, to);
        move.enPassant = true;
        move.enPassantTarget = enPassantTarget;
        return move;
    }

    /**
     * Move of a pone to the last line with turning into another figure.
     * Type of piece to transform into is chosen separately and is Queen by default
     * @param from
     * @param to
     * @param chooseTransformation whether player needs to choose a figure for the pone to become.
     * @return
     */
    static Move poneTransformation(Pos from, Pos to, boolean chooseTransformation) {
        Move move = new Move(FigureType.PONE, from, to);
        move.poneTransformation = true;
        move.chooseTransformation = chooseTransformation;
        return move;
    }

    /**
     * Produces a move string in form that is parsable <b>parseMove</b> method
     * @return string representation of the move
     */
    @Override
    public String toString(){
        if(castle){
            return to.x == 2 ? "0-0-0" : "0-0";
        }
        String res = String.format("%s%s%s", figureType, from, to);
        if(poneTransformation){
            return String.format("%s%s", res, poneTransformationFigure);
        }
        if(enPassant){
            return String.format("%se.p.", res);
        }
        return res;
    }

    /**
     * Parses move from pseudo-PGN notation in full form i.e.
     * [<i>figure letter uppercase</i>(if is not a Pone)][<i>position from</i>][<i>position to</i>][<i>figure to transform into</i>(if is a Pone)]
     * or 0-0-0 or 0-0. If a pone move is followed by e.p. then it is an <i>en passant</i>.
     * @param repr string representing only the move itself (e.g. "0-0" or "e2e4")
     * @param isWhite whether the move is made by a white figure
     * @return move corresponding to the representation
     */
    public static Move parseMove(String repr, boolean isWhite){
        if(repr.equals("0-0-0")){
            if(isWhite){
                return Move.castle(Pos.parse("e1"), Pos.parse("c1"), Pos.parse("a1"), Pos.parse("d1"));
            }
            return Move.castle(Pos.parse("e8"), Pos.parse("c8"), Pos.parse("a8"), Pos.parse("d8"));
        }
        if(repr.equals("0-0")){
            if(isWhite){
                return Move.castle(Pos.parse("e1"), Pos.parse("g1"), Pos.parse("h1"), Pos.parse("f1"));
            }
            return Move.castle(Pos.parse("e8"), Pos.parse("g8"), Pos.parse("h8"), Pos.parse("f8"));
        }
        //regular figure move
        String figId = repr.substring(0, 1);
        if(figId.toUpperCase().equals(figId)){
            Pos from = Pos.parse(repr.substring(1, 3)), to = Pos.parse(repr.substring(3, 5));
            if(from == null || to == null){
                return null;
            }
            return Move.regular(FigureType.parse(figId), from, to);
        }
        //pone
        Pos from = Pos.parse(repr.substring(0,2));
        Pos to = Pos.parse(repr.substring(2, 4));
        if(to == null || from == null){
            return null;
        }
        //en passant
        String postf = repr.substring(4);
        if(postf.equals("e.p.")){
            return enPassant(from, to, new Pos(to.x, from.y));
        }
        //transformation
        if(!postf.isEmpty() && postf.equals(postf.toUpperCase())){
            Move move = poneTransformation(from, to, false);
            move.poneTransformationFigure = FigureType.parse(postf);
            return move;
        }
        //jump
        int dif = to.y - from.y;
        if(dif == 2 || dif == -2){
            return poneJump(from, to, new Pos(from.x, isWhite ? 2 : 5));
        }
        return Move.regular(FigureType.PONE, from, to);
    }

    /**
     * Makes the move on the board given if it is possible
     * @param board board that move is made on
     */
    public void make(Board board) {
        Figure figure = board.getFigure(from);
        if (figure.getType() == FigureType.KING) {
            if (figure.isWhite) {
                board.longCastleWhite = board.shortCastleWhite = false;
            } else {
                board.longCastleBlack = board.shortCastleBlack = false;
            }
        }
        if (figure.getType() == FigureType.ROOK) {
            if (from.is("a1")) {
                board.longCastleWhite = false;
            }
            if (from.is("h1")) {
                board.shortCastleWhite = false;
            }
            if (from.is("a8")) {
                board.longCastleBlack = false;
            }
            if (from.is("h8")) {
                board.shortCastleBlack = false;
            }
        }
        board.setFigure(from, null);
        if(poneTransformation){
            Figure newFigure = poneTransformationFigure.makeInstance(figure.isWhite);
            board.setFigure(to, newFigure);
        }else{
            board.setFigure(to, figure);
        }
        if (castle) {
            Figure rook = board.getFigure(rookFrom);
            board.setFigure(rookFrom, null);
            board.setFigure(rookTo, rook);
        }
        if (enPassant) {
            board.setFigure(enPassantTarget, null);
        }
        board.setPoneJump(poneJumpPos);

        board.clearAttacks();
        board.switchSides();
    }
}
