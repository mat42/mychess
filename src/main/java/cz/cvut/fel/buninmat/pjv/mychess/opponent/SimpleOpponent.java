package cz.cvut.fel.buninmat.pjv.mychess.opponent;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Move;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;

import java.util.LinkedList;
import java.util.Random;

/**
 * Makes random moves from the set of those that are possible
 */
public class SimpleOpponent implements Opponent {
    @Override
    public Move getMove(BoardModel boardModel) {
        LinkedList<Move> moves = new LinkedList<>();
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                Pos pos = new Pos(j, i);
                LinkedList<Move> figureMoves = boardModel.board.getMoves(pos);
                if(figureMoves != null) {
                    moves.addAll(figureMoves);
                }
            }
        }
        if(moves.size() == 0){
            return null;
        }
        int moveNum = new Random().nextInt(moves.size());
        Move move = moves.get(moveNum);
        move.setChooseTransformation(false);
        return move;
    }

    @Override
    public void getMoveAsync(BoardModel boardModel, OnMoveAction onMove) {
        onMove.run(getMove(boardModel));
    }
}
