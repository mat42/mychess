package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

import java.util.LinkedList;

public class Rook extends Figure {
    @Override
    public FigureType getType() {
        return FigureType.ROOK;
    }

    Rook(Boolean white) {
        super(white);
    }

    @Override
    public LinkedList<Move> getMoves(Board board, Pos from) {
        return movesFromAttacksLine(from, board);
    }

    @Override
    public LinkedList<Pos> getAttacks(Pos from, Board board) {
        return attacksLineFromDirs(from,
                new Pos[]{new Pos(-1, 0), new Pos(0, 1), new Pos(1, 0), new Pos(0, -1)},
                board);
    }
}
