package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

import java.util.LinkedList;

public class Queen extends Figure {
    @Override
    public FigureType getType() {
        return FigureType.QUEEN;
    }

    Queen(Boolean white) {
        super(white);
    }

    @Override
    public LinkedList<Move> getMoves(Board board, Pos from) {
        return movesFromAttacksLine(from, board);
    }

    @Override
    public LinkedList<Pos> getAttacks(Pos from, Board board) {
        return attacksLineFromDirs(from, King.moveDirs, board);
    }
}
