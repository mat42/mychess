package cz.cvut.fel.buninmat.pjv.mychess.engine;

import java.util.LinkedList;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

public class Pone extends Figure {
    @Override
    public FigureType getType() {
        return FigureType.PONE;
    }

    Pone(Boolean white){
        super(white);
    }

    @Override
    public LinkedList<Move> getMoves(Board board, Pos from){
        Figure figure = board.getFigure(from);
        LinkedList<Move> moves = new LinkedList<>();

        Pos dir = figure.isWhite ? new Pos(0, 1) : new Pos(0, -1);
        Pos to = from.plus(dir);
        if(board.getFigure(to) == null){
            Move nextMove = transformationMove(figure, to) ?
                    Move.poneTransformation(from, to, true) :
                    Move.regular(getType(), from, to);
            moves.add(nextMove);

            boolean doubleMove = (figure.isWhite && from.y == 1) || (!figure.isWhite && from.y == 6);
            Pos longTo = to.plus(dir);
            if(doubleMove && board.getFigure(longTo) == null){
                moves.add(Move.poneJump(from, longTo, to));
            }
        }
        for(Pos takePos : getAttacks(from, board)){
            Figure target = board.getFigure(takePos);
            if(target != null && target.isWhite != figure.isWhite){
                Move takeMove = transformationMove(figure, to) ?
                        Move.poneTransformation(from, takePos, true) :
                        Move.regular(getType(), from, takePos);
                moves.add(takeMove);
            }
            if(board.getPoneJump() != null && board.getPoneJump().is(takePos)){
                moves.add(Move.enPassant(from, takePos, new Pos(takePos.x, from.y)));
            }
        }
        return moves;
    }

    private boolean transformationMove(Figure figure, Pos to){
        return (to.y == 7 && figure.isWhite) || (to.y == 0 && !figure.isWhite);
    }

    @Override
    public LinkedList<Pos> getAttacks(Pos from, Board board) {
        Pos[] dirs = isWhite ?
                new Pos[]{new Pos(-1, 1), new Pos(1, 1)} :
                new Pos[]{new Pos(-1, -1), new Pos(1, -1)};
        return attacksFromDirs(from, dirs);
    }
}
