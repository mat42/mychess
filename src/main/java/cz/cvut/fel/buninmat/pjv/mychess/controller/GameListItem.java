package cz.cvut.fel.buninmat.pjv.mychess.controller;

import cz.cvut.fel.buninmat.dbs.chessdb.DBManager;
import cz.cvut.fel.buninmat.dbs.chessdb.model.GameMoves;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Opponents;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

/**
 * Controller for GameListItem.fxml user control
 */
public class GameListItem extends HBox {
    @FXML
    private Label blackPlayerLabel;

    @FXML
    private Label whitePlayerLabel;

    @FXML
    private Button removeButton;

    @FXML
    private Button viewButton;

    /**
     *
     * @param opponents opponents that play or played the game
     * @param onView called when "view" button is clicked. Receives list of game moves to display
     */
    public GameListItem(Opponents opponents, Consumer<List<GameMoves>> onView) {
        this.getStylesheets().add("styles/common.css");
        this.getStylesheets().add("styles/game-item.css");
        load();
        whitePlayerLabel.setText(opponents.getPlayer1().getNick());
        blackPlayerLabel.setText(opponents.getPlayer2().getNick());

        String wStyle="", bStyle="";
        switch (opponents.getGame().getResult()){
            case DRAW: wStyle = bStyle = "draw-label";break;
            case WHITE_WIN: wStyle = "won-label"; bStyle = "lost-label";break;
            case BLACK_WIN: wStyle = "lost-label"; bStyle = "won-label";break;
            default:break;
        }
        if(!wStyle.isEmpty()) {
            whitePlayerLabel.getStyleClass().add(wStyle);
            blackPlayerLabel.getStyleClass().add(bStyle);
        }

        if(opponents.getGame().isActive()){
            removeButton.setVisible(false);
        }else {
            removeButton.setOnMouseClicked(event -> {
                DBManager.getInstatnce().removeGame(opponents);
                this.setManaged(false);
                this.setVisible(false);
            });
        }

        viewButton.setOnMouseClicked(event -> {
            onView.accept(DBManager.getInstatnce().getMoves(opponents.getGame()));
        });
    }

    private void load(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/fxml/GameListItem.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

}
