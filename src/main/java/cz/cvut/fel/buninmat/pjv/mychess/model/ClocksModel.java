package cz.cvut.fel.buninmat.pjv.mychess.model;

import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;

/**
 * Represents standard chess clocks with main time limit and addition after each move made. Time is stored in tenth of a second.
 */
public class ClocksModel {

    private int decSec;
    private final int additionSec;

    private Task tickTask;
    private boolean running;

    private Runnable onFinished;
    private StringProperty timeStringProperty;

    private ClocksModel(int decSec, int additionSec) {
        this.decSec = decSec;
        this.additionSec = additionSec;
    }

    void setOnFinished(Runnable onFinished){
        this.onFinished = onFinished;
    }

    public int getTimeDecSec(){
        return decSec;
    }

    boolean isRunning(){
        return running;
    }

    /**
     * Sets what time property to update
     * @param stringProperty dependency property to bind the time string to
     */
    public void setTargetTimeString(StringProperty stringProperty){
        timeStringProperty = stringProperty;
        timeStringProperty.setValue(getTimeString());
    }

    /**
     * Creates standard clocks
     * @param seconds base time limit
     * @param additionSec time addition
     * @return
     */
    public static ClocksModel standard(int seconds, int additionSec){
        return new ClocksModel(seconds * 10, additionSec);
    }

    /**
     * parses clocks time data from string and creates an instance
     * @param string
     * @return clocks instance
     */
    public static ClocksModel fromString(String string) {
        String[] parts = string.split("\\+");
        Integer mils, addition;
        if ((mils = tryParse(parts[0])) == null
                || (addition = tryParse(parts[1])) == null)
            return null;
        return new ClocksModel(mils, addition);
    }

    private static Integer tryParse(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     *
     * @return string bijection in format [tenth of a second]+[addition]
     */
    @Override
    public String toString() {
        return String.format("%d+%d", decSec, additionSec);
    }

    @Override
    public boolean equals(Object obj){
        if(obj.getClass() != getClass()){
            return false;
        }
        ClocksModel other = (ClocksModel)obj;
        return other.additionSec == additionSec && other.decSec == decSec;
    }

    private class TickTask extends Task{

        @Override
        protected Object call() throws Exception {
            while(decSec != 0 && ! isCancelled()){
                try{
                    Thread.sleep(100);
                }catch (InterruptedException e){
                    if(isCancelled()){
                        break;
                    }
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
                decSec--;
                updateMessage(getTimeString());
            }
            return null;
        }

        @Override
        protected void succeeded(){
            super.succeeded();
            if(onFinished != null) {
                onFinished.run();
            }
        }

        @Override
        protected void cancelled() {
            decSec += additionSec * 10;
            updateMessage(getTimeString());
        }
    }

    void start(){
        tickTask = new TickTask();

        if(timeStringProperty != null) {
            timeStringProperty.bind(tickTask.messageProperty());
        }
        Thread thread = new Thread(tickTask);
        thread.setDaemon(true);
        thread.start();
        running = true;
    }

    void stop(){
        if(!running){
            return;
        }
        tickTask.cancel();
        running = false;
    }

    /**
     * String containing clocks info in format "hh:mm::ss+<i>addition</i> " or "mm:ss+<i>addition</i>" for 0 hours
     * @return
     */
    public String descriptiveString(){
        return String.format("%s+%d", getTimeString(), additionSec);
    }

    private String getTimeString() {
        int sec = decSec / 10;

        int min = sec / 60;
        sec %= 60;

        int hrs = min / 60;
        min %= 60;

        if (hrs == 0) {
            return String.format("%d:%d", min, sec);
        }

        return String.format("%d:%d:%d", hrs, min, sec);
    }

}
