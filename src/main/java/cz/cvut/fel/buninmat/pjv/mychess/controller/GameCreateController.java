package cz.cvut.fel.buninmat.pjv.mychess.controller;

import cz.cvut.fel.buninmat.pjv.mychess.Main;
import cz.cvut.fel.buninmat.pjv.mychess.engine.FigureType;
import cz.cvut.fel.buninmat.pjv.mychess.game_saving.GameRecord;
import cz.cvut.fel.buninmat.pjv.mychess.game_saving.GameSaver;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import cz.cvut.fel.buninmat.pjv.mychess.network.NetworkOpponent;
import cz.cvut.fel.buninmat.pjv.mychess.view.board.DragBoardUI;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Board;
import cz.cvut.fel.buninmat.pjv.mychess.model.ClocksModel;
import cz.cvut.fel.buninmat.pjv.mychess.model.WriterModel;
import cz.cvut.fel.buninmat.pjv.mychess.opponent.Opponent;

import java.util.Random;

/**
 * Controller for GameCreateWindow.fxml. Creates BoardModel and initializes a new game
 */
public class GameCreateController {

    @FXML
    private GridPane mainGrid;

    private BoardModel boardModel;

    @FXML
    private Button turnButton;
    @FXML
    private Button goButton;
    @FXML
    private Label errorLabel;
    @FXML
    private ComboBox tempoComboBox;
    @FXML
    private ComboBox additionComboBox;
    @FXML
    private Button colorWhiteButton;
    @FXML
    private Button colorWhiteBlackButton;
    @FXML
    private Button colorBlackButton;
    @FXML
    private Button black00Button, black000Button, white00Button, white000Button;
    @FXML
    private VBox savedGamesBox;
    @FXML
    private Label blackTimeLabel, whiteTimeLabel;
    @FXML
    private HBox tempoHBox, additionHBox;

    private enum ColorType{
        BLACK,
        WHITE,
        RANDOM
    }

    private Button colorButtonSelected;
    private DragBoardUI boardUI;

    private boolean loadedClocks;

    /**
     * Sets an opponent for BoardModel that is created.
     * @param opponent Opponent child class, an opponent to play a game with
     * @see BoardModel
     */
    public void setOpponent(Opponent opponent){
        boardModel.addOpponent(opponent);
    }

    @FXML
    private void initialize(){
        mainGrid.getStylesheets().add("styles/common.css");
        mainGrid.getStylesheets().add("styles/game-create.css");

        goButton.setOnMouseClicked(event->createGame());

        boardModel = new BoardModel(Board.startClassic());
        setupBoardUI(boardModel);

        turnButton.setOnMouseClicked(event -> {
            boardModel.board.switchSides();
            updateTurn();
        });
        updateTurn();

        initComboBox(tempoComboBox);
        initComboBox(additionComboBox);

        colorWhiteButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
                event -> onColorSelected(colorWhiteButton, ColorType.WHITE));

        colorWhiteBlackButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
                event -> onColorSelected(colorWhiteBlackButton, ColorType.RANDOM));

        colorBlackButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
                event -> onColorSelected(colorBlackButton, ColorType.BLACK));
        colorButtonSelected = colorWhiteButton;
        colorButtonSelected.getStyleClass().add("color-button-selected");

        initCastles();
        initSavedGamesList();
    }

    private void initCastles(){
        Board board = boardModel.board;
        initCastleButton(black00Button, board.shortCastleBlack);
        initCastleButton(black000Button, board.longCastleBlack);
        initCastleButton(white00Button, board.shortCastleWhite);
        initCastleButton(white000Button, board.longCastleWhite);
    }

    private void setupBoardUI(BoardModel boardModel){
        if(boardUI != null){
            mainGrid.getChildren().remove(boardUI);
        }
        boardUI = new DragBoardUI(400, 600, boardModel);
        boardUI.setAlignment(Pos.CENTER);
        mainGrid.add(boardUI, 1, 0);
        GridPane.setValignment(boardUI, VPos.TOP);
    }

    private void initCastleButton(Button button, boolean castle){
        button.setOnMouseClicked(event -> onCastleButton(button));
        if(castle) {
            button.getStyleClass().add("color-button-selected");
        }
    }

    private void initComboBox(ComboBox comboBox){
        float[] values = new float[]{0.25f, 0.5f, 0.75f, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 45,
                60, 90, 120, 150, 180};
        TimeComboBoxItem zero = new TimeComboBoxItem(0);
        comboBox.getItems().add(zero);
        comboBox.setValue(zero);
        for(float value : values){
            comboBox.getItems().add(new TimeComboBoxItem(value));
        }
    }

    public class TimeComboBoxItem{
        float value;

        int getSec(){
            return (int)(value * 60);
        }

        TimeComboBoxItem(float value){
            this.value = value;
        }

        @Override
        public String toString() {
            Integer whole = (int)value;
            if(value == whole){
                return whole.toString();
            }
            if(value == 0.25f){
                return "1/4";
            }
            if(value == 0.5f){
                return "1/2";
            }
            if(value == 0.75){
                return "3/4";
            }
            return ((Float)value).toString();
        }
    }

    private Button selectedSavedGameButton;

    private void initSavedGamesList(){
        Button def = new Button();
        def.setText("create new");
        def.getStyleClass().add("color-button");
        def.getStyleClass().add("color-button-selected");
        def.setOnMouseClicked(event -> {
            loadDefault();
            selectedSavedGameButton.getStyleClass().remove(2);
            def.getStyleClass().add("color-button-selected");
            selectedSavedGameButton = def;
        });
        savedGamesBox.getChildren().add(def);

        selectedSavedGameButton = def;

        for(GameRecord record : GameRecord.listRecords()){
            Button recordButton = new Button();
            recordButton.setText(record.getName());
            recordButton.getStyleClass().add("color-button");
            recordButton.setOnMouseClicked(event -> {
                loadGame(record);
                selectedSavedGameButton.getStyleClass().remove(2);
                recordButton.getStyleClass().add("color-button-selected");
                selectedSavedGameButton = recordButton;
            });
            savedGamesBox.getChildren().add(recordButton);
        }
    }

    private void loadGame(GameRecord record){
        boardModel = GameSaver.load(record);
        if(boardModel == null){
            return;
        }
        setupBoardUI(boardModel);
        if(boardModel.opponentWhite()){
            onColorSelected(colorBlackButton, ColorType.BLACK);
        }else{
            onColorSelected(colorWhiteButton, ColorType.WHITE);
        }
        updateCastles();
        updateTurn();
        if(boardModel.hasClocks()){
            setClocksEnabled(false);
            whiteTimeLabel.setText("white: " + boardModel.getWhiteClocks().descriptiveString());
            blackTimeLabel.setText("black: " + boardModel.getBlackClocks().descriptiveString());
            loadedClocks = true;
        }
    }

    private void loadDefault(){
        boardModel = new BoardModel(Board.startClassic());
        setupBoardUI(boardModel);
        onColorSelected(colorWhiteButton, ColorType.WHITE);
        updateCastles();
        updateTurn();
        setClocksEnabled(true);
        loadedClocks = false;
    }

    private void setClocksEnabled(boolean val){
        tempoHBox.setManaged(val);
        tempoHBox.setVisible(val);
        additionHBox.setManaged(val);
        additionHBox.setVisible(val);

        whiteTimeLabel.setManaged(!val);
        whiteTimeLabel.setVisible(!val);
        blackTimeLabel.setManaged(!val);
        blackTimeLabel.setVisible(!val);
    }

    private void updateCastles(){
        Board board = boardModel.board;
        updateCastleButton(black00Button, board.shortCastleBlack);
        updateCastleButton(black000Button, board.longCastleBlack);
        updateCastleButton(white00Button, board.shortCastleWhite);
        updateCastleButton(white000Button, board.longCastleWhite);
    }

    private void onColorSelected(Button button, ColorType colorType){
        colorButtonSelected.getStyleClass().remove(2);
        colorButtonSelected = button;
        button.getStyleClass().add("color-button-selected");
        boolean opponentWhite = colorType == ColorType.BLACK;
        if(colorType == ColorType.RANDOM){
            opponentWhite = new Random().nextInt() % 2 == 0;
        }
        boardModel.setOpponentWhite(opponentWhite);
    }

    private void updateTurn(){
        Image image = new Image(boardModel.board.isWhiteTurn() ?
                FigureType.KING.whiteImageUrl :
                FigureType.KING.blackImageUrl);
        turnButton.setGraphic(new ImageView(image));
    }

    private void onCastleButton(Button button){
        Board board = boardModel.board;
        if(button == white00Button){
            board.shortCastleWhite = ! board.shortCastleWhite;
            updateCastleButton(button, board.shortCastleWhite);
        }
        if(button == white000Button){
            board.longCastleWhite = ! board.longCastleWhite;
            updateCastleButton(button, board.longCastleWhite);
        }
        if(button == black00Button){
            board.shortCastleBlack = ! board.shortCastleBlack;
            updateCastleButton(button, board.shortCastleBlack);
        }
        if(button == black000Button){
            board.longCastleBlack = ! board.longCastleBlack;
            updateCastleButton(button, board.longCastleBlack);
        }
    }

    private void updateCastleButton(Button button, boolean castle){
        if(castle){
            if(button.getStyleClass().size() != 3) {
                button.getStyleClass().add("color-button-selected");
            }
        }else{
            if(button.getStyleClass().size() == 3) {
                button.getStyleClass().remove(2);
            }
        }
    }

    private void createGame(){

        if(!boardModel.board.validPGN()){
            errorLabel.setVisible(true);
            return;
        }

        if(!loadedClocks) {
            TimeComboBoxItem tempoMins = (TimeComboBoxItem) tempoComboBox.getValue();
            TimeComboBoxItem additionMins = (TimeComboBoxItem) additionComboBox.getValue();

            if (tempoMins.getSec() != 0) {

                ClocksModel blackClocksModel = ClocksModel.standard(tempoMins.getSec(), additionMins.getSec());
                ClocksModel whiteClocksModel = ClocksModel.standard(tempoMins.getSec(), additionMins.getSec());

                boardModel.addClocks(blackClocksModel, whiteClocksModel);
            }
        }

        if(boardModel.getWriterModel() == null) {
            boardModel.addWriter(new WriterModel());
        }

        //add a random opponent user
        boardModel.addOpponentUser();

        //make boardModel update Game entity in database
        if(Main.getInstance().getUser() != null){
            boardModel.initDbWriting();
        }

        //send the created board to an opponent
        Opponent opp = boardModel.getOpponent();
        if(opp instanceof NetworkOpponent){
            ((NetworkOpponent)opp).sendGameAsync(boardModel);
        }
        //start game
        Main.getInstance().loadGameControl((Stage) mainGrid.getScene().getWindow(), boardModel);
    }
}
