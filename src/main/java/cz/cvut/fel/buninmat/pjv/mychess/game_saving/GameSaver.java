package cz.cvut.fel.buninmat.pjv.mychess.game_saving;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Figure;
import cz.cvut.fel.buninmat.pjv.mychess.engine.FigureType;
import cz.cvut.fel.buninmat.pjv.mychess.model.BoardModel;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Board;
import cz.cvut.fel.buninmat.pjv.mychess.model.ClocksModel;
import cz.cvut.fel.buninmat.pjv.mychess.model.WriterModel;
import cz.cvut.fel.buninmat.pjv.mychess.network.Communicator;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Reads or writes game representation to a stream or a file.
 */
public class GameSaver {

    private static final Logger LOG = Logger.getLogger(Communicator.class.getName());

    private static BoardModel boardModel;

    /**
     * Loads a saved game corresponding to the record
     * @return BoardModel defining the game
     */
    public static BoardModel load(GameRecord gameRecord){

        File file = new File(gameRecord.getUrl());
        if(!file.exists()){
            return null;
        }

        BoardModel boardModel = null;

        try(BufferedReader br = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8)){
            boardModel = readFromBuffer(br);

        }catch (IOException e){
            LOG.severe(e.getMessage());
        }
        return boardModel;
    }

    /**
     * read game from a stream with the specified BufferedReader
     */
    public static BoardModel readFromBuffer(BufferedReader br){
        WriterModel writerModel = new WriterModel();
        BoardModel boardModel = new BoardModel().addWriter(writerModel);
        try{
            parseFigures(boardModel.board, br.readLine());
            LOG.info("reading model");
            parseFigures(boardModel.board, br.readLine());

            writerModel.setMoveMessages(parseMoves(br.readLine()));

            String clocksStr = br.readLine();
            if(!clocksStr.isEmpty()) {
                ClocksModel clocksWhite, clocksBlack;
                clocksWhite = parseClocks(clocksStr);
                clocksBlack = parseClocks(br.readLine());
                boardModel.addClocks(clocksBlack, clocksWhite);
            }

            parseCastles(br.readLine(), boardModel.board);
            boardModel.board.setTurn(parseColor(br.readLine()));
            boardModel.setOpponentWhite(!parseColor(br.readLine()));
        }catch (IOException e){
            LOG.severe(e.getMessage());
        }
        return boardModel;
    }

    /**
     * Saves the game with the specified name
     * @param name name of game record
     * @param boardModel game to save
     * @return game record with the specified name
     */
    public static GameRecord save(String name, BoardModel boardModel){

        GameRecord gameRecord = new GameRecord(name);

        File file = new File(gameRecord.getUrl());

        try{
            new File(GameRecord.getGamesDirectory()).mkdirs();
            file.createNewFile();
            BufferedWriter bw  = Files.newBufferedWriter(file.toPath(), StandardCharsets.UTF_8);
            writeToBuffer(bw, boardModel);
            bw.close();
        }catch (IOException e){
            LOG.severe(e.toString());
        }

        return gameRecord;
    }

    /**
     * Write the game to a stream with specified BufferedWriter
     */
    public static void writeToBuffer(BufferedWriter bw, BoardModel boardModel){
        GameSaver.boardModel = boardModel;
        try {
            LOG.info("writing model");
            bw.write(getFiguresString(true));
            bw.newLine();
            bw.write(getFiguresString(false));
            bw.newLine();
            bw.write(getMovesString());
            bw.newLine();
            if (boardModel.hasClocks()) {
                bw.write(String.format("timeWhite: %s", boardModel.getWhiteClocks()));
                bw.newLine();
                bw.write(String.format("timeBlack: %s", boardModel.getBlackClocks()));
                bw.newLine();
            } else {
                bw.newLine();
            }
            bw.write(getCastlesString());
            bw.newLine();
            bw.write(String.format("onMove: %s", boardModel.board.isWhiteTurn() ? "white" : "black"));
            bw.newLine();
            bw.write(String.format("playingSide: %s", boardModel.opponentWhite() ? "black" : "white"));
            bw.newLine();
        }catch (IOException e){
            LOG.severe(e.toString());
        }
    }

    private static String getFiguresString(boolean isWhite){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                Pos pos = new Pos(j, i);
                Figure figure = boardModel.board.getFigure(pos);
                if(figure != null && figure.isWhite == isWhite) {
                    stringBuilder.append(String.format("%s%s ", figure.getType().toString(), pos.toString()));
                }
            }
        }

        return String.format("%s %s", isWhite ? "white: " : "black: ", stringBuilder.toString());
    }

    private static String getMovesString(){
        StringBuilder stringBuilder = new StringBuilder();
        int idx = 0;
        if(boardModel.getWriterModel() != null) {
            for (WriterModel.MoveMessage message : boardModel.getWriterModel().getMoveMessages()) {
                idx++;
                stringBuilder.append(String.format("%d. ", idx));
                String typePrefix = "n";
                if (message.type == WriterModel.MoveMessageType.WHITE) {
                    typePrefix = "w";
                }
                if (message.type == WriterModel.MoveMessageType.BLACK) {
                    typePrefix = "b";
                }
                stringBuilder.append(String.format("%s_%s", typePrefix, message.content));
                stringBuilder.append(" ");
            }
        }
        return stringBuilder.toString();
    }

    private static String getCastlesString(){
        StringBuilder sb = new StringBuilder();
        Board board = boardModel.board;
        if(board.shortCastleWhite){
            sb.append("white00 ");
        }
        if(board.longCastleWhite){
            sb.append("white000 ");
        }
        if(board.shortCastleBlack){
            sb.append("black00 ");
        }
        if(board.longCastleBlack){
            sb.append("black000 ");
        }
        return sb.toString();
    }

    private static class ParsedToken{
        FigureType figureType;
        Pos firstPos;
        Pos secondPos;

        boolean isFigurePlacing(){
            return figureType != null && firstPos != null;
        }

        boolean isMove(){
            return isFigurePlacing() && secondPos != null;
        }
    }

    private static void parseFigures(Board board, String string){
        Scanner scan = new Scanner(string).useDelimiter(" ");
        boolean isWhite = scan.next().equals("white:");
        while (scan.hasNext()) {
            String token = scan.next();
            if(token.equals("")){
                continue;
            }
            ParsedToken parsedToken = parseToken(token);
            if(! parsedToken.isFigurePlacing()) {
                System.err.println("Error: bad game file");
                return;
            }
            board.setFigure(parsedToken.firstPos, parsedToken.figureType.makeInstance(isWhite));
        }
    }

    private static LinkedList<WriterModel.MoveMessage> parseMoves(String string){
        LinkedList<WriterModel.MoveMessage> moves = new LinkedList<>();
        Scanner scan = new Scanner(string).useDelimiter(" ");
        while (scan.hasNext()) {
            scan.next();
            if(!scan.hasNext()){
                break;
            }
            String moveStr = scan.next();
            if(moveStr.equals("")){
                continue;
            }
            String[] parts = moveStr.split("_");

            WriterModel.MoveMessageType type;
            if(parts[0].equals("w")){
                type = WriterModel.MoveMessageType.WHITE;
            }
            else if(parts[0].equals("b")){
                type = WriterModel.MoveMessageType.BLACK;
            }else{
                type = WriterModel.MoveMessageType.NEUTRAL;
            }
            moves.add(new WriterModel.MoveMessage(parts[1], type));
        }
        return moves;
    }

    private static ParsedToken parseToken(String str){
        ParsedToken parsedToken = new ParsedToken();
        Scanner scan = new Scanner(str).useDelimiter("");
        String figureRepr = "";
        if(scan.hasNext("[A-Z]")){
            figureRepr = scan.next();
        }
        parsedToken.figureType = FigureType.parse(figureRepr);
        parsedToken.firstPos = Pos.parse(scan.next() + scan.next());
        if(scan.hasNext()){
            parsedToken.secondPos = Pos.parse(scan.next() + scan.next());
        }
        return parsedToken;
    }

    private static ClocksModel parseClocks(String str){
        return ClocksModel.fromString(str.split(" ")[1]);
    }

    private static void parseCastles(String str, Board board){
        board.shortCastleWhite = board.longCastleWhite = board.shortCastleBlack = board.longCastleBlack = false;
        for (String token : str.split(" ")){
            if(token.equals("black00")){
                board.shortCastleBlack = true;
            }
            if(token.equals("black000")){
                board.longCastleBlack = true;
            }
            if(token.equals("white00")){
                board.shortCastleWhite = true;
            }
            if(token.equals("white000")){
                board.longCastleWhite = true;
            }
        }
    }

    private static boolean parseColor(String str){
        return str.split(" ")[1].equals("white");
    }
}
