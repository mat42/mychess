package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

import java.util.LinkedList;

/**
 * Represents and manages board state
 */
public class Board {
    private Figure[][] figures;
    private boolean[][] attacks;
    private boolean whiteTurn;
    private Pos poneJumpPos;
    private Pos checkPos;
    private boolean stall;

    public boolean longCastleBlack, shortCastleBlack,
            longCastleWhite, shortCastleWhite;

    private LinkedList<FigureType> whiteCounterweight;
    private LinkedList<FigureType> blackCounterweight;

    public Board() {
        figures = new Figure[8][8];
        attacks = new boolean[8][8];
        whiteCounterweight = new LinkedList<>();
        blackCounterweight = new LinkedList<>();
    }

    /**
     * Cloning constructor
     */
    public Board(Board board) {
        figures = board.figures.clone();
        attacks = board.attacks.clone();
        for(int i = 0; i < 8; i++){
            figures[i] = board.figures[i].clone();
            attacks[i] = board.attacks[i].clone();
        }
        whiteTurn = board.whiteTurn;
        poneJumpPos = board.poneJumpPos;
        checkPos = board.checkPos;
        stall = board.stall;
        longCastleBlack = board.longCastleBlack;
        longCastleWhite = board.longCastleWhite;
        shortCastleWhite = board.shortCastleWhite;
        shortCastleBlack = board.shortCastleBlack;
        whiteCounterweight = new LinkedList<>(board.whiteCounterweight);
        blackCounterweight = new LinkedList<>(board.blackCounterweight);
    }

    public Figure getFigure(Pos pos){
        return figures[pos.y][pos.x];
    }

    public Pos getCheckPos(){
        return checkPos;
    }

    public void setCheckPos(Pos pos){checkPos = pos;}

    Pos getPoneJump(){return poneJumpPos;}

    void setPoneJump(Pos pos){ poneJumpPos = pos;}

    public boolean isMate(){
        return stall && checkPos != null;
    }

    public boolean isStall(){
        return stall;
    }

    public boolean isCheck(){
        return checkPos != null;
    }

    public boolean isWhiteTurn(){
        return whiteTurn;
    }

    public void switchSides(){
        whiteTurn = ! whiteTurn;
    }

    public void setTurn(boolean isWhite){
        whiteTurn = isWhite;
    }

    public void setFigure(Pos pos, Figure fig){
        figures[pos.y][pos.x] = fig;
    }

    public void setAttack(Pos pos){
        attacks[pos.y][pos.x] = true;
    }

    boolean hasAttack(Pos pos){
        return attacks[pos.y][pos.x];
    }

    public void setStall(boolean value){
        stall = value;
    }

    public LinkedList<FigureType> getWhiteCounterweight(){
        updateCounterweights();
        return whiteCounterweight;
    }

    public LinkedList<FigureType> getBlackCounterweight(){
        updateCounterweights();
        return blackCounterweight;
    }

    @Override
    public boolean equals(Object obj){
        if(obj.getClass() != getClass()){
            return false;
        }
        Board other = (Board) obj;
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++) {
                if (figures[i][j] != other.figures[i][j]) {
                    return false;
                }
            }
        }
        return whiteTurn == other.whiteTurn
                && longCastleBlack == other.longCastleBlack
                && longCastleWhite == other.longCastleWhite
                && shortCastleWhite == other.shortCastleWhite
                && shortCastleBlack == other.shortCastleBlack;
    }

    private void updateCounterweights(){
        whiteCounterweight.clear();
        blackCounterweight.clear();
        for(Figure[] row : figures){
            for(Figure figure : row){
                if(figure == null){
                    continue;
                }
                LinkedList<FigureType> counterweight;
                LinkedList<FigureType> opponentCounterweight;
                if(figure.isWhite){
                    counterweight = whiteCounterweight;
                    opponentCounterweight = blackCounterweight;
                }else {
                    counterweight = blackCounterweight;
                    opponentCounterweight = whiteCounterweight;
                }
                if(opponentCounterweight.contains(figure.getType())){
                    opponentCounterweight.remove(figure.getType());
                }else{
                    counterweight.add(figure.getType());
                }
            }
        }
    }

    void clearAttacks(){
        for(int i = 0; i < 8; i++){
            for(int  j = 0; j < 8; j++){
                attacks[i][j] = false;
            }
        }
    }

    /**
     * Board with classical chess start position
     */
    public static Board startClassic(){
        FigureType[] placing = new FigureType[]{
                FigureType.ROOK, FigureType.KNIGHT, FigureType.BISHOP, FigureType.QUEEN,
                FigureType.KING, FigureType.BISHOP, FigureType.KNIGHT, FigureType.ROOK};
        Board board = new Board();
        for(int i = 0; i < 8; i++){
            board.setFigure(new Pos(i, 0), placing[i].makeInstance(true));
            board.setFigure(new Pos(i, 7), placing[i].makeInstance(false));
            board.setFigure(new Pos(i, 1), FigureType.PONE.makeInstance(true));
            board.setFigure(new Pos(i, 6), FigureType.PONE.makeInstance(false));
        }
        board.shortCastleBlack = board.longCastleBlack = board.shortCastleWhite = board.longCastleWhite = true;
        board.whiteTurn = true;
        return board;
    }

    /**
     * Checks weather board is valid: no pone is on the last line, kings are placed and no check is on the board.
     */
    public boolean validPGN(){

        boolean whiteKingPlaced = false, blackKingPlaced = false;

        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                Figure figure = getFigure(new Pos(j, i));
                if(figure == null){
                    continue;
                }
                if(figure.getType() == FigureType.PONE && (i == 0 || i == 7) ){
                    return false;
                }
                if(figure.getType() == FigureType.KING){
                    if(figure.isWhite){
                        if(whiteKingPlaced){
                            return false;
                        }
                        whiteKingPlaced = true;
                    }else{
                        if(blackKingPlaced){
                            return false;
                        }
                        blackKingPlaced = true;
                    }
                }
            }
        }

        if(!whiteKingPlaced || !blackKingPlaced){
            return false;
        }

        boolean noCheck = getCheck() == null;
        switchSides();
        boolean noOpponentCheck = getCheck() == null;
        switchSides();

        return noCheck && noOpponentCheck;
    }

    /**
     *
     * @return position of a King that is attacked or null if there is no check
     */
    public Pos getCheck() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Pos pos = new Pos(j, i);
                Figure fig = getFigure(pos);
                if (fig != null && fig.isWhite == isWhiteTurn()) {
                    for (Pos attack : fig.getAttacks(pos, this)) {
                        Figure target = getFigure(attack);
                        if (target != null &&
                                target.isWhite != isWhiteTurn() &&
                                target.getType() == FigureType.KING) {
                            return attack;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     *
     * @param from position of a figure to make a move
     * @return all possible moves from certain position
     */
    public LinkedList<Move> getMoves(Pos from) {
        Figure figure = getFigure(from);
        if (figure == null || figure.isWhite != isWhiteTurn()) {
            return null;
        }
        if (figure.getType() == FigureType.KING) {
            setAttacks();
        }
        LinkedList<Move> moves = new LinkedList<>();
        for (Move move : figure.getMoves(this, from)) {
            if(possibleMove(move)){
                moves.add(move);
            }
        }
        return moves;
    }

    private boolean possibleMove(Move move){
        Board testBoard = new Board(this);
        move.make(testBoard);
        return testBoard.getCheck() == null;
    }


    private void setAttacks(){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Pos pos = new Pos(j, i);
                Figure fig = getFigure(pos);
                if (fig != null && fig.isWhite != isWhiteTurn()) {
                    for (Pos attack : fig.getAttacks(pos, this)) {
                        setAttack(attack);
                    }
                }
            }
        }
    }

    /**
     *
     * @return <i>true</i> if there is some possible move of the side that is on turn, <i>false</i> otherwise
     */
    public boolean hasMove(){
        setAttacks();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Pos pos = new Pos(j, i);
                Figure figure = getFigure(pos);
                if(figure != null && figure.isWhite == isWhiteTurn()){
                    for(Move move : getMoves(pos)){
                        if(possibleMove(move)){
                            clearAttacks();
                            return true;
                        }
                    }
                }
            }
        }
        clearAttacks();
        return false;
    }
}
