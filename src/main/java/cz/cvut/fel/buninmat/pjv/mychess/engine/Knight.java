package cz.cvut.fel.buninmat.pjv.mychess.engine;

import cz.cvut.fel.buninmat.pjv.mychess.code.Pos;

import java.util.LinkedList;

public class Knight extends Figure {
    @Override
    public FigureType getType() {
        return FigureType.KNIGHT;
    }

    Knight(Boolean white) {
        super(white);
    }

    @Override
    public LinkedList<Move> getMoves(Board board, Pos from) {
        LinkedList<Move> moves = new LinkedList<>();
        Figure figure = board.getFigure(from);
        for(Pos to : getAttacks(from, board)) {
            Figure target = board.getFigure(to);
            if (target == null || target.isWhite != figure.isWhite) {
                moves.add(Move.regular(getType(), from, to));
            }
        }
        return moves;
    }

    @Override
    public LinkedList<Pos> getAttacks(Pos from, Board board) {
        Pos dir1 = new Pos(2, 1);
        Pos dir2 = new Pos(1, 2);
        Pos[] dirs = new Pos[]{
                dir1, dir2,
                dir1.flipX(), dir2.flipX(),
                dir1.flipY(), dir2.flipY(),
                dir1.flipXY(), dir2.flipXY()
        };
        return attacksFromDirs(from, dirs);
    }
}
