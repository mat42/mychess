package cz.cvut.fel.buninmat.pjv.mychess.view;

import cz.cvut.fel.buninmat.pjv.mychess.model.WriterModel;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

/**
 * User control for displaying moves
 */
public class MovesPane extends FlowPane {

    public void addMoveLabel(String message, WriterModel.MoveMessageType type){

        Label lbl = new Label();
        String style;
        if (type == WriterModel.MoveMessageType.BLACK) {
            style = "white-move-label";
        } else if (type == WriterModel.MoveMessageType.WHITE) {
            style = "black-move-label";
        } else {
            style = "move-label";
        }
        lbl.getStyleClass().add(style);
        lbl.setText(message);
        FlowPane.setMargin(lbl, new Insets(4, 4, 4, 4));
        lbl.setPadding(new Insets(2, 2, 2, 2));
        getChildren().add(lbl);
    }

    public void clear(){
        this.getChildren().clear();
    }
}
