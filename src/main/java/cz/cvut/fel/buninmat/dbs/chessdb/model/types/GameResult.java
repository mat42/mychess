package cz.cvut.fel.buninmat.dbs.chessdb.model.types;

public enum GameResult {
    NONE, //still playing
    BLACK_WIN,
    WHITE_WIN,
    DRAW
}
