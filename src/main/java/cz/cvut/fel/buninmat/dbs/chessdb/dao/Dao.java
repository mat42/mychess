package cz.cvut.fel.buninmat.dbs.chessdb.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<T, K> {
    T get(K id);

    List<T> getAll();

    void save(T t);

    void update(T t, String[] params);

    void delete(T t);

    void merge(T t);
}
