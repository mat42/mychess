package cz.cvut.fel.buninmat.dbs.chessdb.dao;

import cz.cvut.fel.buninmat.dbs.chessdb.model.Game;
import cz.cvut.fel.buninmat.dbs.chessdb.model.GameMoveId;
import cz.cvut.fel.buninmat.dbs.chessdb.model.GameMoves;

import javax.persistence.EntityManager;
import java.util.List;

public class GameMovesDao implements Dao<GameMoves, GameMoveId> {

    private EntityManager em;

    public GameMovesDao(EntityManager em) {
        this.em = em;
    }

    @Override
    public GameMoves get(GameMoveId id) {
        return em.find(GameMoves.class, id);
    }

    @Override
    public List<GameMoves> getAll() {
        return null;
    }

    public List<GameMoves> getAll(Game game){
        return em.createQuery("select m from GameMoves m where m.game=:game", GameMoves.class)
                .setParameter("game", game)
                .getResultList();
    }

    @Override
    public void save(GameMoves gameMoves) {
        em.persist(gameMoves);
    }

    @Override
    public void update(GameMoves gameMoves, String[] params) {

    }

    @Override
    public void delete(GameMoves gameMoves) {
        em.remove(gameMoves);
    }

    @Override
    public void merge(GameMoves gameMoves) {}
}
