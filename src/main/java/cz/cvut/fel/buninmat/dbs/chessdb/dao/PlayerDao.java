package cz.cvut.fel.buninmat.dbs.chessdb.dao;

import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;

import javax.persistence.EntityManager;
import java.util.List;

public class PlayerDao implements Dao<Player, String> {

    private EntityManager em;

    public PlayerDao(EntityManager em){
        this.em = em;
    }

    @Override
    public Player get(String id) {
        return em.find(Player.class, id);
    }

    @Override
    public List<Player> getAll() {
        return em.createQuery("select p from Player p", Player.class).getResultList();
    }

    @Override
    public void save(Player player) {
        em.persist(player);
    }

    @Override
    public void update(Player player, String[] params) {

    }

    @Override
    public void delete(Player player) {

    }

    @Override
    public void merge(Player player) {

    }
}
