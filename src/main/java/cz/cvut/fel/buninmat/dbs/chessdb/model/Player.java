package cz.cvut.fel.buninmat.dbs.chessdb.model;

import javax.persistence.*;

@Entity
@Table(
        name = "player",
        uniqueConstraints =
                @UniqueConstraint(columnNames = {"email"})
)
public class Player {

    @Id
    private String nick;

    private String email;

    private int passwordHash;

    public Player() {
    }

    public Player(String nick, String email, int passwordHash) {
        this.nick = nick;
        this.email = email;
        this.passwordHash = passwordHash;
    }

    public String getNick() {
        return nick;
    }

    public String getEmail() {
        return email;
    }

    public int getPasswordHash() {
        return passwordHash;
    }
}
