package cz.cvut.fel.buninmat.dbs.chessdb.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class GameMoveId implements Serializable {

    //Looks like there is no way to create composed foreign key with @ManyToOne

    private long gameId;

    private String moveStr;

    private int moveNum;

    public GameMoveId() {
    }

    public GameMoveId(Game game, String moveStr, int moveNum) {
        this.gameId = game.getGameId();
        this.moveStr = moveStr;
        this.moveNum = moveNum;
    }

    public long getGameId() {
        return gameId;
    }

    public String getMoveStr() {
        return moveStr;
    }

    public int getMoveNum() {
        return moveNum;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public void setMoveStr(String moveStr) {
        this.moveStr = moveStr;
    }

    public void setMoveNum(int moveNum) {
        this.moveNum = moveNum;
    }
}
