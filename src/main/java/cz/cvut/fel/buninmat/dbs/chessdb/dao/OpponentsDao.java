package cz.cvut.fel.buninmat.dbs.chessdb.dao;

import cz.cvut.fel.buninmat.dbs.chessdb.model.Opponents;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;

import javax.persistence.EntityManager;

import java.util.List;

public class OpponentsDao implements Dao<Opponents, Long> {
    private EntityManager em;

    public OpponentsDao(EntityManager em) {
        this.em = em;
    }

    @Override
    public Opponents get(Long id) {
        return em.find(Opponents.class, id);
    }

    @Override
    public List<Opponents> getAll() { return em.createQuery("select o from Opponents o", Opponents.class).getResultList();}

    public List<Opponents> getAll(Player player) {
        return em.createQuery("select o from Opponents o where o.player1 = :player or o.player2 = :player", Opponents.class)
                .setParameter("player", player)
                .getResultList();
    }

    @Override
    public void save(Opponents opponents) {
        em.persist(opponents);
    }

    @Override
    public void update(Opponents opponents, String[] params) {}

    @Override
    public void delete(Opponents opponents) {
        em.remove(opponents);
    }

    @Override
    public void merge(Opponents opponents) {}
}
