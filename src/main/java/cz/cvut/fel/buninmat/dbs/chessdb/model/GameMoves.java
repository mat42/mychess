package cz.cvut.fel.buninmat.dbs.chessdb.model;

import javax.persistence.*;

@Entity
public class GameMoves {
    @EmbeddedId
    private GameMoveId gameMoveId;
    @ManyToOne
    private Game game;

    public GameMoves() {
    }

    public GameMoves(GameMoveId gameMoveId, Game game) {
        this.gameMoveId = gameMoveId;
        this.game = game;
    }

    public GameMoveId getGameMoveId() {
        return gameMoveId;
    }
}
