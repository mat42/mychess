package cz.cvut.fel.buninmat.dbs.chessdb.model;

import cz.cvut.fel.buninmat.dbs.chessdb.model.types.GameResult;

import javax.persistence.*;

@Entity
public class Game  {
    @Id
    @GeneratedValue
    private long gameId;

    private boolean rated;

    private boolean active;

    @Enumerated(value=EnumType.STRING)
    private GameResult result;

    public Game() {
    }

    public Game(boolean rated, boolean active, GameResult result) {
        this.rated = rated;
        this.active = active;
        this.result = result;
    }

    public long getGameId() {
        return gameId;
    }

    public boolean isRated() {
        return rated;
    }

    public boolean isActive() {
        return active;
    }

    public GameResult getResult() {
        return result;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public void setRated(boolean rated) {
        this.rated = rated;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setResult(GameResult result) {
        this.result = result;
    }
}
