package cz.cvut.fel.buninmat.dbs.chessdb.dao;

import cz.cvut.fel.buninmat.dbs.chessdb.model.Game;
import cz.cvut.fel.buninmat.dbs.chessdb.model.Player;

import javax.persistence.EntityManager;
import java.util.List;

public class GameDao implements Dao<Game, Long> {

    private EntityManager em;

    public GameDao(EntityManager em) {
        this.em = em;
    }

    @Override
    public Game get(Long id) {
        return em.find(Game.class, id);
    }

    @Override
    public List<Game> getAll() {
        return em.createQuery("select g from Game g", Game.class).getResultList();
    }

    public List<Game> getAll(Player player) {
        return em.createQuery("select o.game from Opponents o where o.player1 = :player", Game.class)
                .setParameter("player", player)
                .getResultList();
    }

    @Override
    public void save(Game game) {
        em.persist(game);
    }

    @Override
    public void update(Game game, String[] params) {

    }

    @Override
    public void delete(Game game) {
        em.remove(game);
    }

    @Override
    public void merge(Game game) {
        em.merge(game);
    }
}
