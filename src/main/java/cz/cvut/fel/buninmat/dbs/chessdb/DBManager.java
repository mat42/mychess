package cz.cvut.fel.buninmat.dbs.chessdb;

import cz.cvut.fel.buninmat.dbs.chessdb.dao.GameDao;
import cz.cvut.fel.buninmat.dbs.chessdb.dao.GameMovesDao;
import cz.cvut.fel.buninmat.dbs.chessdb.dao.OpponentsDao;
import cz.cvut.fel.buninmat.dbs.chessdb.dao.PlayerDao;
import cz.cvut.fel.buninmat.dbs.chessdb.model.*;
import cz.cvut.fel.buninmat.pjv.mychess.engine.Move;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Manages database connection
 */
public class DBManager {

    private static final Logger LOG = Logger.getLogger(DBManager.class.getName());

    private EntityManagerFactory emf;
    private EntityManager em;

    private PlayerDao playerDao;
    private GameDao gameDao;
    private GameMovesDao gameMovesDao;
    private OpponentsDao opponentsDao;

    private boolean isSetup;

    private static DBManager instance = new DBManager();

    public static DBManager getInstatnce(){
        return instance;
    }

    /**
     *
     * @return weather connection was successfully set up
     */
    public boolean isSetup() {
        return isSetup;
    }

    /**
     * Creates database persistence context
     */
    public void setup(){
        try {
            emf = Persistence.createEntityManagerFactory("pu");
            em = emf.createEntityManager();
            playerDao = new PlayerDao(em);
            gameDao = new GameDao(em);
            opponentsDao = new OpponentsDao(em);
            gameMovesDao = new GameMovesDao(em);
            isSetup = true;
        }catch (PersistenceException e){
            isSetup = false;
            LOG.warning("No database connection");
        }
    }

    /**
     * Adds some entities to database
     */
    public void init(){
        if(! isSetup){
            return;
        }
        addPlayers();
    }

    private void addPlayers(){
        Player[] players = {
                new Player("admin", "admin@mail.cz", "admin".hashCode()),
                new Player("alice", "alice@mail.cz", "alice".hashCode()),
                new Player("bob", "bob@mail.cz", "bob".hashCode())
        };

        //add players that are not added
        for(Player player : players){
            em.getTransaction().begin();
            Player existing = playerDao.get(player.getNick());
            if(existing == null){
                playerDao.save(player);
            }
            em.getTransaction().commit();
        }
    }

    /**
     *
     * @return Player instance if authentication was successful, null otherwise
     */
    public Player authenticate(String username, int passwordHash){
        if(! isSetup){
            return null;
        }
        Player player = playerDao.get(username);
        if(player != null && player.getPasswordHash() == passwordHash){
            return player;
        }
        return null;
    }

    /**
     *
     * @return random player instance from Players table
     */
    public Player randomPlayer(){
        if(! isSetup){
            return null;
        }
        em.getTransaction().begin();
        List<Player> players = playerDao.getAll();
        em.getTransaction().commit();

        //randomly select a player
        Random random = new Random();
        int randInd = random.nextInt(players.size());
        return players.get(randInd);
    }

    /**
     * Adds a new game to database
     * @param game
     */
    public void submitGame(Game game){
        if(! isSetup){
            return;
        }
        //create game if not exist else update
        em.getTransaction().begin();
        Game existing = gameDao.get(game.getGameId());
        if(existing == null){
            gameDao.save(game);
        }else{
            gameDao.merge(game);
        }
        em.getTransaction().commit();
    }

    /**
     * Adds the relationship of players playing the game
     * @param game
     * @param player1
     * @param player2
     */
    public void addPlayersPair(Game game, Player player1, Player player2){
        if(! isSetup){
            return;
        }
        em.getTransaction().begin();
        Opponents existing = opponentsDao.get(game.getGameId());
        if(existing == null){
            opponentsDao.save(new Opponents(game, player1, player2));
        }
        em.getTransaction().commit();
    }

    /**
     * Adds a new move of some game
     * @param game
     * @param move
     * @param moveNumber
     */
    public void submitMove(Game game, Move move, int moveNumber){
        if(!isSetup){
            return;
        }
        em.getTransaction().begin();
        gameMovesDao.save(new GameMoves(new GameMoveId(game, move.toString(), moveNumber), game));
        em.getTransaction().commit();
    }

    /**
     * Deletes a completed game
     * @param opponents
     */
    public void removeGame(Opponents opponents){
        if(!isSetup){
            return;
        }
        em.getTransaction().begin();
        List<GameMoves> moves = gameMovesDao.getAll(opponents.getGame());
        for (GameMoves move : moves){
            gameMovesDao.delete(move);
        }
        //game is then deleted by cascade
        opponentsDao.delete(opponents);
        em.getTransaction().commit();
    }

    /**
     * Gets opponents of a player that he ever played with
     * @param player
     * @return
     */
    public List<Opponents> getOpponents(Player player){
        if(! isSetup){
            return new ArrayList<>();
        }
        em.getTransaction().begin();
        List<Opponents> opponents = opponentsDao.getAll(player);
        em.getTransaction().commit();
        return opponents;
    }

    /**
     * All moves of the specified game
     * @param game
     * @return
     */
    public List<GameMoves> getMoves(Game game){
        if(! isSetup){
            return new ArrayList<>();
        }
        em.getTransaction().begin();
        List<GameMoves> opponents = gameMovesDao.getAll(game);
        em.getTransaction().commit();
        return opponents;
    }

    public void close(){
        em.close();
        emf.close();
    }


}
