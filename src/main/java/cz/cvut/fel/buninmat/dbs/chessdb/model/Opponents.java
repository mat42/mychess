package cz.cvut.fel.buninmat.dbs.chessdb.model;

import javax.persistence.*;

@Entity
public class Opponents {
    @Id
    @OneToOne(cascade = {CascadeType.REMOVE})
    private Game game;

    @ManyToOne
    private Player player1;

    @ManyToOne
    private Player player2;

    public Opponents(){}

    public Opponents(Game game, Player player1, Player player2) {
        this.game = game;
        this.player1 = player1;
        this.player2 = player2;
    }

    public Game getGame() {
        return game;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }
}
