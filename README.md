# MyChess

Desktop app in JavaFX. JRE 1.8.0 is required.

## Features:

- Fully implemented chess rules
- Board editor
- User profiles
- Local game
- Clocks
- Kinda-PGN
- Playing through sockets

## Usage
### First steps
![Start window](screenshots/start-window.png)

After running the application if you have eyes you should see three big buttons with pictures on them. So counting them from left to right:
- Click the first button if you want to play with yourself in a sandbox mode.
- Click the middle button if you want to play with an AI.
- Click the third button if you want to play through network.
- Click the cross button in the corner if you don't want to play at all* (recomended)*.
You may have also noticed a small **Login** button. After clicking it you get a dialog to input your credentals (tip: username "admin" with password "admin" will always work). If dialog says *"No DB connection"* then you do not loose a lot, so close it and do something else.
Remember that on which button you click depends your further fate as a user of this application.

### Board editor

![Board editor](screenshots/board-editor.png)

Now you may choose a startup configuration for your game. Drag any chess piece you see anywhere you want. If you drag a piece outside of the board you throw it away if you put it on the board it is placed there. Click buttons on the pane in the left to load some of saved configurations. On the right pane you choose:
- Who is on move
- If you want to play with clocks, then set time and addition
- The side you want to play. The middle king makes the choise for you.
- Choose what castles are allowed (for advanced players only).
Then click **Go** to start the game.

### Game

![Game](screenshots/game-window.png)

This part is intuitive if you play chess. To save the game in it's current state you click the save icon and type name of the game, then click **Ok**.

### Profile

![Profile](screenshots/profile.png)

If your login was successful then you can visit your profile by clicking the button with your username on start screen. Here you see all the games you played. The winning side is highlighted  green and games with an undefined result are not highlighted. You can delete a game by clicking an **X** button if the game is closed or view it's moves by clicking **View** button. 